<?php

namespace ShrooPHP\Framework\Uploads;

use ShrooPHP\Core\Upload as IUpload;

class Upload implements IUpload
{
	/**
	 * @var string the name of the upload
	 */
	private $name;

	/**
	 * @var string the type of the upload
	 */
	private $type;

	/**
	 * @var int the size of the upload
	 */
	private $size;

	/**
	 * @var string the temporary name of the upload
	 */
	private $path;

	/**
	 * @var int the error associated with the upload
	 */
	private $error;

	/**
	 * Constructs an upload.
	 *
	 * @param string $name the name of the upload
	 * @param string $type the type of the upload
	 * @param int $size the size of the upload
	 * @param string $path the temporary name of the upload
	 * @param int $error the error associated with the upload
	 */
	public function __construct($name, $type, $size, $path, $error)
	{
		$this->setName($name);
		$this->setType($type);
		$this->setSize($size);
		$this->setPath($path);
		$this->setError($error);
	}

	/**
	 * Sets the name of the upload.
	 *
	 * @param string $name the  of the upload
	 * @return \ShrooPHP\Framework\Uploads\Upload the upload (for method
	 * chaining)
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Sets the type of the upload.
	 *
	 * @param string $type the type of the upload
	 * @return \ShrooPHP\Framework\Uploads\Upload the upload (for method
	 * chaining)
	 */
	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * Sets the size of the upload.
	 *
	 * @param int $size the size of the upload
	 * @return \ShrooPHP\Framework\Uploads\Upload the upload (for method
	 * chaining)
	 */
	public function setSize($size)
	{
		$this->size = $size;

		return $this;
	}

	/**
	 * Sets the temporary name of the upload.
	 *
	 * @param string $path the temporary name of the upload
	 * @return \ShrooPHP\Framework\Uploads\Upload the upload (for method
	 * chaining)
	 */
	public function setPath($path)
	{
		$this->path = $path;

		return $this;
	}

	/**
	 * Sets the error associated with the upload.
	 *
	 * @param int $error the error associated with the upload
	 * @return \ShrooPHP\Framework\Uploads\Upload the upload (for method
	 * chaining)
	 */
	public function setError($error)
	{
		$this->error = $error;

		return $this;
	}

	public function name()
	{
		return $this->name;
	}

	public function type()
	{
		return $this->type;
	}

	public function size()
	{
		return $this->size;
	}

	public function path()
	{
		return $this->path;
	}

	public function error()
	{
		return $this->error;
	}

}

