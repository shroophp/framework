<?php

namespace ShrooPHP\Framework\Request\Response\Presenters;

use ShrooPHP\Core\Request\Response;
use ShrooPHP\Core\Request\Response\Presenter as IPresenter;
use ShrooPHP\Core\Runnable;
use Traversable;

/**
 * A presenter of responses.
 */
class Presenter implements IPresenter
{
	/**
	 * @var array the headers last sent by the presenter for the previous
	 * response
	 */
	private $sent = array();

	/**
	 * Gets the the headers last sent by the presenter for the previous
	 * response.
	 *
	 * This method is a workaround for the fact that no headers are registered
	 * as being sent by PHP during unit tests.
	 *
	 * @return array the headers last sent by the presenter for the previous
	 * response
	 */
	public function sent()
	{
		return $this->sent;
	}

	public function present(Response $response)
	{
		$this->code($response->code());
		$this->headers($response->headers());
		$this->content($response->content());
	}

	/**
	 * Presents the given response code.
	 *
	 * @param int|null $code the code to present (if any)
	 */
	private function code($code)
	{
		if (!is_null($code)) {
			http_response_code($code);
		}
	}

	/**
	 * Presents the given response headers.
	 *
	 * @param \Traversable $headers the headers to present
	 */
	private function headers(Traversable $headers)
	{
		$this->sent = array();

		foreach ($headers as $header => $value) {
			$concatenated = "{$header}: {$value}";
			header($concatenated);
			$this->sent[] = $concatenated;
		}
	}

	/**
	 * Presents the given response content.
	 *
	 * @param \ShrooPHP\Core\Runnable|null $content the content to present (if
	 * any)
	 */
	private function content(Runnable $content = null)
	{
		if (!is_null($content)) {
			$content->run();
		}
	}
}
