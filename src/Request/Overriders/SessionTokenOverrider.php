<?php

namespace ShrooPHP\Framework\Request\Overriders;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Overrider;
use ShrooPHP\Framework\Requests\TokenRequest;
use ShrooPHP\Core\Token\Generator;
use ShrooPHP\Framework\Tokens\SessionToken;

/**
 * An overrider of request tokens via the associated session.
 */
class SessionTokenOverrider implements Overrider
{
	/**
	 * @var scalar the data key being associated with the token to be validated
	 */
	private $key;

	/**
	 * @var \ShrooPHP\Core\Token\Generator the generator being used to
	 * generate tokens
	 */
	private $generator;

	/**
	 * Constructs an overrider of request tokens via the associated session.
	 *
	 * @param scalar $key the data key to associate with the token being
	 * validated
	 * @param \ShrooPHP\Core\Token\Generator $generator the generator to use
	 * when generating tokens
	 */
	public function __construct($key, Generator $generator)
	{
		$this->key = $key;
		$this->generator = $generator;
	}

	public function override(Request $request)
	{
		$session = $request->session();
		$actual = $request->data()->get($this->key, '');
		$token = new SessionToken($session, "{$actual}", $this->generator);

		return new TokenRequest($request, $token);
	}
}
