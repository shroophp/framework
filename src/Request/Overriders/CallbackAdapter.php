<?php

namespace ShrooPHP\Framework\Request\Overriders;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Overrider;

/**
 * An adapter for an overrider callback.
 */
class CallbackAdapter implements Overrider
{
	/**
	 * @var callable the callback being adapted
	 */
	private $callback;

	/**
	 * Constructs a request overrider adapter for the given callback.
	 *
	 * @param callable $callback the callback to adapt
	 */
	public function __construct($callback)
	{
		$this->callback = $callback;
	}

	public function override(Request $request)
	{
		return call_user_func($this->callback, $request);
	}

}

