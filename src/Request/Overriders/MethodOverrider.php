<?php

namespace ShrooPHP\Framework\Request\Overriders;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Overrider;
use ShrooPHP\Framework\Requests\MethodRequest;

/**
 * A overrider for extracting the method of a request from its data.
 */
class MethodOverrider implements Overrider
{

	/**
	 * @var scalar $key the key being associated with the method
	 */
	private $key;

	/**
	 * Constructs a overrider that extracts the method from the data associated
	 * with the request.
	 *
	 * @param scalar $key the key to associate with the method
	 */
	public function __construct($key)
	{
		$this->key = $key;
	}

	public function override(Request $request)
	{
		$method = $request->data()->get($this->key);

		if (!is_null($method)) {
			$request = new MethodRequest($request, $method);
		}

		return $request;
	}

}
