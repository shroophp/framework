<?php

namespace ShrooPHP\Framework\Request\Handlers;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Core\Request\Validator;

/**
 * A handler that ensures requests are valid before handling them.
 */
class ValidatedHandler implements Handler
{
	/**
	 * @var \ShrooPHP\Core\Request\Validator the current validator
	 */
	private $validator;

	/**
	 * @var \ShrooPHP\Core\Request\Handler the current handler
	 */
	private $handler;

	/**
	 * Constructs a handler that ensures requests are valid before handling
	 * them.
	 *
	 * @param \ShrooPHP\Core\Request\Validator $validator the validator to
	 * validate requests
	 * @param \ShrooPHP\Core\Request\Handler $handler the handler to handle
	 * valid requests
	 */
	public function __construct(Validator $validator, Handler $handler)
	{
		$this->validator = $validator;
		$this->handler = $handler;
	}

	public function handle(Request $request)
	{
		$handled = $this->validator->validate($request);

		if ($handled) {
			$handled = $this->handler->handle($request);
		}

		return $handled;
	}
}
