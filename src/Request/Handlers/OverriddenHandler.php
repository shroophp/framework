<?php

namespace ShrooPHP\Framework\Request\Handlers;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Core\Request\Overrider;

class OverriddenHandler implements Handler
{
	private $overrider;

	private $handler;

	public function __construct(Overrider $overrider, Handler $handler)
	{
		$this->overrider = $overrider;
		$this->handler = $handler;
	}

	public function handle(Request $request)
	{
		$overridden = $this->overrider->override($request);

		return $this->handler->handle($overridden);
	}
}
