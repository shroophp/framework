<?php

namespace ShrooPHP\Framework\Request\Handlers;

use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Core\Pattern\Interpretation;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Framework\Requests\ArgsRequest;

/**
 * A handler associated with an interpretation.
 */
class InterpretationHandler implements Handler
{
	/**
	 * @var \ShrooPHP\Core\Pattern\Interpretation the interpretation
	 * being associated with the handler
	 */
	private $interpretation;

	/**
	 * @var \ShrooPHP\Core\Request\Handler the handler being associated with
	 * the interpretation
	 */
	private $handler;

	/**
	 *
	 * @param Interpretation $interpretation
	 * @param Handler $handler
	 */
	public function __construct(
		Interpretation $interpretation,
		Handler $handler
	) {
		$this->interpretation = $interpretation;
		$this->handler = $handler;
	}

	public function handle(Request $request)
	{
		$handled = false;
		$args = $this->interpretation->match($request->path());

		if (!is_null($args)) {
			$handled = $this->override($request, $args);
		}

		return $handled;
	}

	/**
	 * Overrides the given request with the given arguments before passing them
	 * to the current handler.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to override
	 * @param array $args the arguments to associate with the given request
	 * @return bool whether or not the given request was handled
	 */
	private function override(Request $request, array $args)
	{
		$converted = new ImmutableArrayObject($args);
		$overidden = new ArgsRequest($request, $converted);

		return $this->handler->handle($overidden);
	}

}
