<?php

namespace ShrooPHP\Framework\Request\Handlers;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler;

/**
 * Combines multiple request handlers so as to be used as a single request
 * handler.
 */
class AggregateHandler implements Handler
{

	/**
	 * @var array the handlers being aggregated
	 */
	private $handlers = array();

	/**
	 * Pushes the given handler onto the aggregate.
	 *
	 * @param \ShrooPHP\Core\Request\Handler $handler the handler to push
	 */
	public function push(Handler $handler)
	{
		$this->handlers[] = $handler;
	}

	public function handle(Request $request)
	{
		$handled = false;

		foreach ($this->handlers as $handler) {

			$handled = $handler->handle($request);

			if ($handled) {
				break;
			}
		}

		return $handled;
	}
}
