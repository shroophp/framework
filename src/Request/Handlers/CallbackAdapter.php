<?php

namespace ShrooPHP\Framework\Request\Handlers;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Core\Request\Response;
use ShrooPHP\Core\Request\Response\Presenter;

/**
 * An adapter for a handler callback.
 */
class CallbackAdapter implements Handler
{
	/**
	 * @var callable the callback being adapted
	 */
	private $callback;

	/**
	 * @var \ShrooPHP\Core\Request\Response\Presenter the presenter being used
	 * to present responses
	 */
	private $presenter;

	/**
	 * Constructs a handler adapter for the given callback.
	 *
	 * @param callable $callback the callback to adapt
	 * @param \ShrooPHP\Core\Request\Response\Presenter the presenter to use
	 * when presenting responses
	 */
	public function __construct($callback, Presenter $presenter)
	{
		$this->callback = $callback;
		$this->presenter = $presenter;
	}

	public function handle(Request $request)
	{
		$response = call_user_func($this->callback, $request);

		if ($response instanceof Response) {
			$this->presenter->present($response);
		}

		return (bool) $response;
	}
}

