<?php

namespace ShrooPHP\Framework\Request\Responses;

use ShrooPHP\Core\KeyValuePairIterator;
use ShrooPHP\Core\Pairs\Pair;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Runnable;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Core\Request\Response as IResponse;
use ShrooPHP\Core\Request\Response\Presenter as IPresenter;
use ShrooPHP\Core\Runnables\CallbackAdapter;
use ShrooPHP\Core\Runnables\ValueAdapter;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Application\Modifier;
use ShrooPHP\Framework\Request\Response\Presenters\Presenter;

/**
 * A response to a request.
 */
class Response implements Handler, IResponse, Modifier, Runnable
{

	// https://tools.ietf.org/html/rfc7231#section-6

	const HTTP_CONTINUE = 100;
	const HTTP_SWITCHING_PROTOCOLS = 101;

	const HTTP_OK = 200;
	const HTTP_CREATED = 201;
	const HTTP_ACCEPTED = 202;
	const HTTP_NON_AUTHORITATIVE_INFORMATION = 203;
	const HTTP_NO_CONTENT = 204;
	const HTTP_RESET_CONTENT = 205;
	const HTTP_PARTIAL_CONTENT = 206;

	const HTTP_MULTIPLE_CHOICES = 300;
	const HTTP_MOVED_PERMANENTLY = 301;
	const HTTP_FOUND = 302;
	const HTTP_SEE_OTHER = 303;
	const HTTP_NOT_MODIFIED = 304;
	const HTTP_USE_PROXY = 305;
	const HTTP_SWITCH_PROXY = 306;
	const HTTP_TEMPORARY_REDIRECT = 307;

	const HTTP_BAD_REQUEST = 400;
	const HTTP_UNAUTHORIZED = 401;
	const HTTP_PAYMENT_REQUIRED = 402;
	const HTTP_FORBIDDEN = 403;
	const HTTP_NOT_FOUND = 404;
	const HTTP_METHOD_NOT_ALLOWED = 405;
	const HTTP_NOT_ACCEPTABLE = 406;
	const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
	const HTTP_REQUEST_TIMEOUT = 408;
	const HTTP_CONFLICT = 409;
	const HTTP_GONE = 410;
	const HTTP_LENGH_REQUIRED = 411;
	const HTTP_PRECONDITION_FAILED = 412;
	const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;
	const HTTP_REQUEST_URI_TOO_LONG = 414;
	const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;
	const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
	const HTTP_EXPECTATION_FAILED = 417;
	const HTTP_I_AM_A_TEAPOT = 418;

	const HTTP_INTERNAL_SERVER_ERROR = 500;
	const HTTP_NOT_IMPLEMENTED = 501;
	const HTTP_BAD_GATEWAY = 502;
	const HTTP_SERVICE_UNAVAILABLE = 503;
	const HTTP_GATEWAY_TIMEOUT = 504;
	const HTTP_HTTP_VERSION_NOT_SUPPORTED = 505;

	/**
	 * @var int|null $code the status code of the response (if any)
	 */
	private $code;

	/**
	 * @var array $headers the headers of the response (as an array of pairs)
	 */
	private $headers = array();

	/**
	 * @var \ShrooPHP\Core\Runnable|null $content the content of the response
	 * (if any)
	 */
	private $content;

	/**
	 * @var \ShrooPHP\Core\Request\Response\Presenter $presenter the presenter
	 * to use when the response presents itself
	 */
	private $presenter;

	/**
	 * Creates a response by converting the given callback to content.
	 *
	 * @param \ShrooPHP\Core\Runnable|callable $callback the callback to convert
	 * @param string|null $type the type of the content (if any)
	 * @return \ShrooPHP\Framework\Request\Responses\Response the converted
	 * callback
	 */
	public static function callback($callback, $type = null)
	{
		if (!($callback instanceof Runnable)) {
			$callback = new CallbackAdapter($callback);
		}

		return new self(null, self::toHeaders($type), $callback);
	}

	/**
	 * Creates a response by converting the given string to content.
	 *
	 * @param string $string the string to convert
	 * @param string|null $type the type of the content (if any)
	 * @return \ShrooPHP\Framework\Request\Responses\Response the converted
	 * string
	 */
	public static function string($string, $type = null)
	{
		$headers = self::toHeaders($type);
		return new self(null, $headers, new ValueAdapter($string));
	}

	/**
	 * Converts the given content type to headers.
	 *
	 * @param string|null $type the value to convert
	 * @return array|null the converted value
	 */
	private static function toHeaders($type)
	{
		$headers = null;

		if (!is_null($type)) {
			$headers = array('Content-Type' => $type);
		}

		return $headers;
	}

	/**
	 * Constructs a response to a request.
	 *
	 * @param int|null $code the status code of the response
	 * @param \Traversable|array|null $headers the headers of the response
	 * @param \ShrooPHP\Core\Runnable|null $content the content of the
	 * response
	 * @param \ShrooPHP\Core\Request\Response\Presenter $presenter the
	 * presenter of the response
	 */
	public function __construct(
		$code = null,
		$headers = null,
		Runnable $content = null,
		IPresenter $presenter = null
	) {
		$this->setCode($code);
		$this->resetHeaders($headers);
		$this->setContent($content);
		$this->setPresenter(is_null($presenter) ? new Presenter : $presenter);
	}

	/**
	 * Sets the status code of the response.
	 *
	 * @param int|null $code the status code of the response
	 * @return \ShrooPHP\Framework\Request\Responses\Response the response (for
	 * method chaining)
	 */
	public function setCode($code)
	{
		$this->code = $code;

		return $this;
	}

	/**
	 * Sets the content of the response.
	 *
	 * @param \ShrooPHP\Core\Runnable $content the content of the response
	 * @return \ShrooPHP\Framework\Request\Responses\Response the response (for
	 * method chaining)
	 */
	public function setContent(Runnable $content = null)
	{
		$this->content = $content;

		return $this;
	}

	/**
	 * Sets the presenter of the response.
	 *
	 * @param \ShrooPHP\Core\Request\Response\Presenter $presenter the
	 * presenter of the response
	 * @return \ShrooPHP\Framework\Request\Responses\Response the response (for
	 * method chaining)
	 */
	public function setPresenter(IPresenter $presenter)
	{
		$this->presenter = $presenter;

		return $this;
	}

	/**
	 * Clear all headers and reset them to those given (if any).
	 *
	 * @param \Traversable|array|null $headers the headers to set on the
	 * response (if any).
	 * @return \ShrooPHP\Framework\Request\Responses\Response the response (for
	 * method chaining)
	 */
	public function resetHeaders($headers = null)
	{
		$this->headers = array();

		if (!is_null($headers)) {
			foreach ($headers as $name => $value) {
				$this->addHeader($name, $value);
			}
		}

		return $this;
	}

	/**
	 * Add a header to the response.
	 *
	 * @param string $name the name of the header
	 * @param string $value the value of the header
	 * @return \ShrooPHP\Framework\Request\Responses\Response the response (for
	 * method chaining)
	 */
	public function addHeader($name, $value)
	{
		$this->headers[] = new Pair($name, $value);
		return $this;
	}

	public function code()
	{
		return $this->code;
	}

	public function headers()
	{
		return new KeyValuePairIterator($this->headers);
	}

	public function content()
	{
		return $this->content;
	}

	public function run()
	{
		$this->presenter->present($this);
	}

	public function handle(Request $request)
	{
		$this->run();
		return true;
	}

	public function respond(Request $request, array $args)
	{
		return $this;
	}

	public function modify(Application $app)
	{
		$app->push($this);
	}

}
