<?php

namespace ShrooPHP\Framework\Request\Validators;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Validator;

/**
 * A validator of request methods.
 */
class MethodValidator implements Validator
{
	/**
	 * @var array the methods being considered valid (stored as array keys for
	 * fast retrieval)
	 */
	private $methods;

	/**
	 * Constructs a validator of request methods.
	 *
	 * @param array $methods the methods to consider valid
	 */
	public function __construct(array $methods)
	{
		$this->methods = array_fill_keys($methods, true);
	}

	public function validate(Request $request)
	{
		return isset($this->methods[$request->method()]);
	}
}
