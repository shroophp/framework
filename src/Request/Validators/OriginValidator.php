<?php

namespace ShrooPHP\Framework\Request\Validators;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Validator;

/**
 * A validator for the origins of requests.
 */
class OriginValidator implements Validator
{
	/**
	 * @var string the currently expected source origin
	 */
	private $target;

	/**
	 * Constructs a validator for the origins of requests.
	 *
	 * @param string $target the expected source origin of validated requests
	 */
	public function __construct($target)
	{
		$this->target = $target;
	}

	public function validate(Request $request)
	{
		$origin = null;
		$referer = null;

		foreach ($request->headers() as $header => $value) {

			switch (strtolower(trim($header))) {
				case 'origin':
					$origin = $value;
					break;
				case 'referer':
					$referer = $value;
					break;
			}

			if (!is_null($origin)) {
				break;
			}
		}

		return is_null($origin)
			? $this->matches($referer)
			: $this->matches($origin);
	}

	/**
	 * Determines whether or not the given source origin matches the current
	 * target origin.
	 *
	 * @param string $source the source origin to evaluate
	 * @return bool whether or not the given source origin matches the current
	 * target origin
	 */
	private function matches($source)
	{
		return !is_null($source) && $source === $this->target;
	}
}
