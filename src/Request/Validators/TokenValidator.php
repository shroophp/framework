<?php

namespace ShrooPHP\Framework\Request\Validators;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Validator;

/**
 * A validator of request tokens.
 */
class TokenValidator implements Validator
{
	public function validate(Request $request)
	{
		return $request->token()->valid();
	}

}
