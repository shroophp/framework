<?php

namespace ShrooPHP\Framework\Request\Validators;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Validator;

/**
 * An adapter for a validator callback.
 */
class CallbackAdapter implements Validator
{

	/**
	 * @var callable the callback being adapted
	 */
	private $callback;

	/**
	 * Constructs a request validator adapter for the given callback.
	 *
	 * @param callable $callback the callback to adapt
	 */
	public function __construct($callback)
	{
		$this->callback = $callback;
	}

	public function validate(Request $request)
	{
		return call_user_func($this->callback, $request);
	}

}
