<?php

namespace ShrooPHP\Framework\Tokens;

use ShrooPHP\Core\Session;
use ShrooPHP\Core\Token;
use ShrooPHP\Core\Token\Generator;
use ShrooPHP\Framework\Token\Generators\RandomGenerator;

/**
 * A token assoicated with a session.
 */
class SessionToken implements Token
{
	/**
	 * The key being used to store the token within the session.
	 */
	const KEY = __CLASS__;

	/**
	 * @var \ShrooPHP\Core\Session the session being used to store the expected
	 * token
	 */
	private $session;

	/**
	 * @var string the token currently being compared to the expected token
	 */
	private $actual;

	/**
	 * @var \ShrooPHP\Core\Token\Generator the generator being used to generate
	 * tokens
	 */
	private $generator;

	/**
	 * Constructs a token that is associated with the given session.
	 *
	 * @param \ShrooPHP\Core\Session $session the session being associated with
	 * the token
	 * @param string $actual the token to compare to the session token during
	 * validation
	 * @param \ShrooPHP\Core\Token\Generator $generator the generator to use in
	 * order to generate the token
	 */
	public function __construct(
		Session $session,
		$actual,
		Generator $generator
	) {

		$this->session = $session;
		$this->actual = $actual;
		$this->generator = $generator;
	}

	public function expected()
	{
		$expected = $this->session->get(self::KEY);

		if (is_null($expected)) {
			$expected = $this->generator->generate();
			$this->session->set(self::KEY, $expected);
			$this->session->commit();
		}

		return $expected;
	}

	public function valid()
	{
		return $this->expected() === $this->actual;
	}
}
