<?php

namespace ShrooPHP\Framework\Tokens;

use ShrooPHP\Core\Token;

/**
 * An empty and invalid token.
 */
class EmptyToken implements Token
{
	public function expected()
	{
		return '';
	}

	public function valid()
	{
		return false;
	}

}

