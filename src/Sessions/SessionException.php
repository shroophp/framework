<?php

namespace ShrooPHP\Framework\Sessions;

use Exception;
use RuntimeException;

/**
 * An exception relating to sessions.
 */
class SessionException extends RuntimeException
{
	/**
	 * the value of the code indicating that sessions are disabled
	 */
	const DISABLED = 1;

	/**
	 * the value of the code indicating that the session could not be started
	 */
	const CANNOT_START = 2;

	/**
	 * Constructs an exception relating to sessions.
	 *
	 * @param int $code the exception code (as one of the class constant
	 * values)
	 * @param Exception $previous the previous exception used for the exception
	 * chaining
	 */
	public function __construct($code, Exception $previous = null)
	{
		parent::__construct(self::toMessage($code), $code, $previous);
	}

	/**
	 * Converts the given class constant value to its respective message.
	 *
	 * @param int $code the class constant value to convert
	 * @return string the converted class constant value
	 */
	private static function toMessage($code)
	{
		$message = '';

		switch($code) {
			case self::DISABLED:
				$message .= 'Sessions disabled';
				break;
			case self::CANNOT_START:
				$message .= 'Failed to start';
				break;
		}

		return $message;
	}
}
