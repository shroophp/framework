<?php

namespace ShrooPHP\Framework\Sessions;

use ShrooPHP\Core\Session as ISession;
use ShrooPHP\Framework\Sessions\SessionException;

class Session implements ISession
{

	/**
	 * @var bool whether or not the session has been started
	 */
	private $started = false;

	public function get($key, $default = null)
	{
		$this->startIfNotStarted();
		return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
	}

	public function set($key, $value)
	{
		$this->activateIfInactive();
		$_SESSION[$key] = $value;
	}

	public function commit()
	{
		$this->startIfNotStarted();
		session_write_close();
	}

	/**
	 * Activates the session if it is not currently active.
	 */
	private function activateIfInactive()
	{
		if (!$this->isActive()) {
			$this->startIfNotStarted();
		}
	}

	/**
	 * Starts the session if it has not been started.
	 */
	private function startIfNotStarted()
	{
		if (!$this->hasStarted()) {
			$this->start();
			$this->hasStarted(true);
		}
	}

	/**
	 * Gets (and optionally sets) whether or not the session has been started.
	 *
	 * @param bool|null $started whether or not the session has been started
	 * @return bool whether or not the session has been started
	 */
	private function hasStarted($started = null)
	{
		$previous = $this->started;
		if (!is_null($started)) {
			$this->started = !!$started;
		}
		return $previous;
	}

	/**
	 * Determines whether or not the session is active.
	 *
	 * @return bool whether or not the session is active
	 * @throws \ShrooPHP\Framework\Sessions\SessionException sessions are
	 * disabled
	 */
	private function isActive()
	{
		$active = false;

		switch (session_status()) {
			case PHP_SESSION_DISABLED:
				throw new SessionException(SessionException::DISABLED);
			case PHP_SESSION_ACTIVE:
				$active = true;
				break;
		}

		return $active;
	}

	/**
	 * Starts the session.
	 *
	 * @throws \ShrooPHP\Framework\Sessions\SessionException the session cannot
	 * be started
	 */
	private function start()
	{
		if (!session_start()) {
			throw new SessionException(SessionException::CANNOT_START);
		}
		$this->hasStarted(true);
	}
}
