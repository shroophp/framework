<?php

namespace ShrooPHP\Framework\Application\Modifiers;

use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Core\Request\Response\Presenter;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Application\Modifier;
use ShrooPHP\Framework\Request\Handlers\CallbackAdapter as HandlerCallbackAdapter;

/**
 * Adapts a handler so as to be used as an application modifier.
 */
class HandlerAdapter implements Modifier
{
	/**
	 * @var \ShrooPHP\Core\Handler the handler being adapted
	 */
	private $handler;

	/**
	 * Creates an application modifier adapter for the given request handler.
	 *
	 * @param \ShrooPHP\Core\Request\Handler|callable $handler the handler to
	 * adapt
	 * @param \ShrooPHP\Core\Request\Response\Presenter $presenter the
	 * presenter to use when presenting responses
	 * @return \ShrooPHP\Framework\Application\Modifiers\CallabckAdapter the
	 * adapted handler
	 */
	public static function create($handler, Presenter $presenter)
	{
		if (!($handler instanceof Handler)) {
			$handler = new HandlerCallbackAdapter($handler, $presenter);
		}

		return new self($handler);
	}

	/**
	 * Constructs an application modifier adapter for the given request
	 * handler.
	 *
	 * @param \ShrooPHP\Core\Request\Handler $handler the handler to adapt
	 */
	public function __construct(Handler $handler)
	{
		$this->handler = $handler;
	}

	public function modify(Application $app)
	{
		$app->push($this->handler);
	}
}
