<?php

namespace ShrooPHP\Framework\Application\Modifiers;

use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Application\Modifier;

/**
 * Adapts a callback so as to be used as an application modifier.
 */
class CallbackAdapter implements Modifier
{
	/**
	 * @var callable the callback being adapted
	 */
	private $callback;

	/**
	 * Constructs an application modifier adapter for the given callback.
	 *
	 * @param callable $callback the callback to adapt
	 */
	public function __construct(callable $callback)
	{
		$this->callback = $callback;
	}

	public function modify(Application $app)
	{
		call_user_func($this->callback, $app);
	}

}

