<?php

namespace ShrooPHP\Framework\Application;

use ShrooPHP\Framework\Application;

/**
 * A modifier of applications.
 */
interface Modifier
{
	/**
	 * Modifies the given application.
	 *
	 * @param \ShrooPHP\Framework\Application $app the application to modify
	 */
	public function modify(Application $app);
}

