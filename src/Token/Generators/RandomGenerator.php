<?php

namespace ShrooPHP\Framework\Token\Generators;

use ShrooPHP\Core\Token\Generator;

/**
 * A generator of tokens via random bytes.
 */
class RandomGenerator implements Generator
{
	/**
	 * @var int the number of bytes currently being used to generate tokens
	 */
	private $bytes;

	/**
	 * Constructs a generator of tokens using the given number of random bytes.
	 *
	 * @param int $bytes the number of bytes to use in order to generate tokens
	 */
	public function __construct($bytes)
	{
		$this->bytes = $bytes;
	}

	public function generate()
	{
		return bin2hex(random_bytes($this->bytes));
	}
}
