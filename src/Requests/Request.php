<?php

namespace ShrooPHP\Framework\Requests;

use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Core\ReadOnlyArrayObject;
use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\Session as ISession;
use ShrooPHP\Core\Openable;
use ShrooPHP\Core\Token;
use ShrooPHP\Framework\Sessions\Session;
use ShrooPHP\Framework\Tokens\EmptyToken;

/**
 * A request.
 */
class Request implements IRequest
{
	/**
	 * The URL of the stream representing the default body.
	 */
	const BODY = 'php://input';

	/**
	 * @var string the method of the request
	 */
	private $method;

	/**
	 * @var string the path of the request
	 */
	private $path;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the URL parameters of the
	 * request
	 */
	private $params;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the headers of the request
	 */
	private $headers;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the cookies of the request
	 */
	private $cookies;

	/**
	 * @var \ShrooPHP\Core\Session the session associated with the
	 * request
	 */
	private $session;

	/**
	 * @var \ShrooPHP\Core\Openable the stream representing the body of the
	 * request
	 */
	private $body;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the data associated with the
	 * body of the request
	 */
	private $data;

	/**
	 * @var array the files associated with the request
	 */
	private $uploads;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the arguments associated with
	 * the request
	 */
	private $args;

	/**
	 * @var \ShrooPHP\Core\Token the token associated with the request
	 */
	private $token;

	/**
	 * Converts the given request URI to a request path.
	 *
	 * @param string $uri the request URI to convert
	 * @return string the converted request URI
	 * @throws \ShrooPHP\Framework\Requests\RequestException the URI could not
	 * be converted
	 */
	public static function toPath($uri)
	{
		$matches = null;

		preg_match('/^(.*?)(\?.*)?$/', $uri, $matches);

		return $matches[1];
	}

	/**
	 * Constructs a request.
	 *
	 * @param string $method the method of the request
	 * @param string $path the path of the request
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $params the URL parameters of
	 * the request
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $headers the headers of the
	 * request
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $cookies the cookies of the
	 * request
	 * @param \ShrooPHP\Core\Session $session the session associated with the
	 * request
	 * @param \ShrooPHP\Core\Openable $body the stream representing the body of
	 * the request
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $data the data associated with
	 * the body of the request
	 * @param array $uploads the files associated with the request
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $args the arguments associated
	 * with the request
	 * @param \ShrooPHP\Core\Token $token the token associated with the request
	 */
	public function __construct(
		$method,
		$path,
		ReadOnlyArrayObject $params = null,
		ReadOnlyArrayObject $headers = null,
		ReadOnlyArrayObject $cookies = null,
		ISession $session = null,
		Openable $body = null,
		ReadOnlyArrayObject $data = null,
		array $uploads = null,
		ReadOnlyArrayObject $args = null,
		Token $token = null
	) {
		$this->setMethod($method);
		$this->setPath($path);
		$this->setParams($this->toImmutableArrayObject($params));
		$this->setHeaders($this->toImmutableArrayObject($headers));
		$this->setCookies($this->toImmutableArrayObject($cookies));
		$this->setSession($this->toSession($session));
		$this->setBody($body);
		$this->setData($this->toImmutableArrayObject($data));
		$this->setUploads($this->toUploads($uploads));
		$this->setArgs($this->toImmutableArrayObject($args));
		$this->setToken($this->toToken($token));
	}

	/**
	 * Sets the method of the request.
	 *
	 * @param string $method the method of the request
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setMethod($method)
	{
		$this->method = $method;

		return $this;
	}

	/**
	 * Sets the path of the request.
	 *
	 * @param string $path the path of the request
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setPath($path)
	{
		$this->path = $path;

		return $this;
	}

	/**
	 * Sets the URL parameters of the request.
	 *
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $params the URL parameters of
	 * the request
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setParams(ReadOnlyArrayObject $params)
	{
		$this->params = $params;

		return $this;
	}

	/**
	 * Sets the headers of the request.
	 *
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $headers the headers of the
	 * request
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setHeaders(ReadOnlyArrayObject $headers)
	{
		$this->headers = $headers;

		return $this;
	}

	/**
	 * Sets the cookies of the request.
	 *
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $cookies the cookies of the
	 * request
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setCookies(ReadOnlyArrayObject $cookies)
	{
		$this->cookies = $cookies;

		return $this;
	}

	/**
	 * Sets the session associated with the request.
	 *
	 * @param \ShrooPHP\Core\Session $session the session associated of the
	 * request
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setSession(ISession $session)
	{
		$this->session = $session;

		return $this;
	}

	/**
	 * Sets the stream representing the body of the request.
	 *
	 * @param \ShrooPHP\Core\Openable|null $body the stream representing the
	 * body of the request (or NULL to use the default)
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setBody(Openable $body = null)
	{
		$this->body = $body;

		return $this;
	}

	/**
	 * Sets the data associated with the body of the request.
	 *
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $data the data associated with
	 * the body of the request
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setData(ReadOnlyArrayObject $data)
	{
		$this->data = $data;

		return $this;
	}

	/**
	 * Sets the files associated with the request
	 *
	 * @param array $uploads the files associated with the request.
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setUploads(array $uploads)
	{
		$this->uploads = $uploads;

		return $this;
	}

	/**
	 * Sets the arguments associated with the request.
	 *
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $args the arguments associated
	 * with the request.
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setArgs(ReadOnlyArrayObject $args)
	{
		$this->args = $args;

		return $this;
	}

	/**
	 * Sets the token associated with the request.
	 *
	 * @param \ShrooPHP\Framework\Requests\Token $token the token associated
	 * with the request
	 * @return \ShrooPHP\Framework\Requests\Request the request (for method
	 * chaining)
	 */
	public function setToken(Token $token)
	{
		$this->token = $token;

		return $this;
	}

	public function method()
	{
		return $this->method;
	}

	public function path()
	{
		return $this->path;
	}

	public function params()
	{
		return $this->params;
	}

	public function headers()
	{
		return $this->headers;
	}

	public function cookies()
	{
		return $this->cookies;
	}

	public function session()
	{
		return $this->session;
	}

	public function open()
	{
		$body = is_null($this->body)
			? fopen(self::BODY, 'rb')
			: $this->body->open();

		return is_resource($body) ? $body : null;
	}

	public function data()
	{
		return $this->data;
	}

	public function uploads()
	{
		return $this->uploads;
	}

	public function args()
	{
		return $this->args;
	}

	public function token()
	{
		return $this->token;
	}

	public function root()
	{
		return $this;
	}

	/**
	 * Returns either the given read-only array object (if specified) or an
	 * empty read-only array object.
	 *
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject|null $default the read-only
	 * array object to return
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject either the given read-only
	 * array object (if specified) or an empty read-only array object
	 */
	private function toImmutableArrayObject(ReadOnlyArrayObject $default = null)
	{
		if (is_null($default)) {
			$default = new ImmutableArrayObject;
		}

		return $default;
	}

	/**
	 * Returns either the given session (if specified) or the default session.
	 *
	 * @param \ShrooPHP\Core\Session|null $session the session to return
	 * @return \ShrooPHP\Core\Session either the given session (if specified)
	 * or the default session
	 */
	private function toSession(ISession $session = null)
	{
		if (is_null($session)) {
			$session = new Session;
		}

		return $session;
	}

	/**
	 * Returns either the given token (if specified) or an empty token.
	 *
	 * @param \ShrooPHP\Core\Token|null $token the token to return
	 * @return \ShrooPHP\Core\Token either the given token (if specified) or an
	 * empty token
	 */
	private function toToken(Token $token = null)
	{
		if (is_null($token)) {
			$token = new EmptyToken;
		}

		return $token;
	}

	/**
	 * Returns either the given array (if specified) or an empty array.
	 *
	 * @param array|null $uploads the array to return
	 * @return array either the given array (if specified) or an empty array
	 */
	private function toUploads(array $uploads = null)
	{
		if (is_null($uploads)) {
			$uploads = array();
		}

		return $uploads;
	}
}
