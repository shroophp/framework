<?php

namespace ShrooPHP\Framework\Requests;

use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\RequestTrait;

/**
 * A request being associated with a specific method.
 */
class MethodRequest implements IRequest
{
	use RequestTrait;

	/**
	 * @var \ShrooPHP\Core\Request the request that is having a specific method
	 * associated with it
	 */
	private $request;

	/**
	 * @var string the method being associated with the request
	 */
	private $method;

	/**
	 * Constructs an association between the given request and the given
	 * method.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to associate with a
	 * specific method
	 * @param string $method the method to associate with the request
	 */
	public function __construct(IRequest $request, $method)
	{
		$this->request = $request;
		$this->method = $method;
	}

	public function method()
	{
		return $this->method;
	}

	protected function request()
	{
		return $this->request;
	}
}
