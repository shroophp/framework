<?php

namespace ShrooPHP\Framework\Requests;

use ShrooPHP\Core\ReadOnlyArrayObject;
use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\RequestTrait;

/**
 * A request being associated with specific arguments.
 */
class ArgsRequest implements IRequest
{
	use RequestTrait;

	/**
	 * @var \ShrooPHP\Core\Request the request that is having specific
	 * arguments associated with it
	 */
	private $request;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the arguments being associated
	 * with the request
	 */
	private $args;

	/**
	 * Constructs an association between the given request and the given
	 * arguments.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to associate with
	 * specific arguments
	 * @param \ShrooPHP\Core\ReadOnlyArrayObject $args the arguments to
	 * associate with the request
	 */
	public function __construct(IRequest $request, ReadOnlyArrayObject $args)
	{
		$this->request = $request;
		$this->args = $args;
	}

	public function args()
	{
		return $this->args;
	}

	protected function request()
	{
		return $this->request;
	}
}
