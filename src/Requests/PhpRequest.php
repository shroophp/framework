<?php

namespace ShrooPHP\Framework\Requests;

use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Framework\Sessions\Session;
use ShrooPHP\Framework\Tokens\EmptyToken;
use ShrooPHP\Framework\Uploads\Upload;

/**
 * A request based upon the server API.
 */
class PhpRequest implements IRequest
{
	/**
	 * The URL of the stream representing the body.
	 */
	const BODY = 'php://input';

	/**
	 * @var string the method of the request
	 */
	private $method;

	/**
	 * @var string the path of the request
	 */
	private $path;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject|null the URL parameters of the
	 * request (or NULL if they are yet to be determined)
	 */
	private $params;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject|null the headers of the request
	 * (or NULL if they are yet to be determined)
	 */
	private $headers;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject|null the cookies of the request
	 * (or NULL if they are yet to be determined)
	 */
	private $cookies;

	/**
	 * @var \ShrooPHP\Core\Session|null the session associated with the request
	 * (or NULL if it is yet to be instantiated)
	 */
	private $session;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject|null the data associated with
	 * the body of the request (or NULL if it is yet to be determined)
	 */
	private $data;

	/**
	 * @var array the uploads associated with the request (or NULL if they are
	 * yet to be determined)
	 */
	private $uploads;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the arguments associated with
	 * the request (or NULL if they are yet to be determined)
	 */
	private $args;

	/**
	 * @var \ShrooPHP\Core\Token the token associated with the request (or NULL
	 * if it is yet to be determined)
	 */
	private $token;

	/**
	 * Generates a request based on the current environment.
	 *
	 * @return \ShrooPHP\Core\Request the generated request
	 * @throws \ShrooPHP\Framework\Requests\PhpRequestException a necessary
	 * environment variable is undefined
	 */
	public static function create()
	{
		$code = PhpRequestException::SAPI;

		if (!isset($_SERVER['REQUEST_METHOD'])) {
			$code |= PhpRequestException::SAPI_REQUEST_METHOD;
		}

		if (!isset($_SERVER['REQUEST_URI'])) {
			$code |= PhpRequestException::SAPI_REQUEST_URI;
		}

		if (PhpRequestException::SAPI ^ $code) {
			throw new PhpRequestException($code);
		}

		$method = $_SERVER['REQUEST_METHOD'];
		$uri = $_SERVER['REQUEST_URI'];

		return new PhpRequest($method, Request::toPath($uri));
	}

	/**
	 * Retrieves all HTTP headers from the current request.
	 *
	 * @return array an associative array of all the HTTP headers in the
	 * current request
	 * @throws \ShrooPHP\Framework\Requests\PhpRequestException failure to
	 * retrieve headers
	 */
	public static function toHeaders()
	{
		if (!function_exists('getallheaders')) {
			throw new PhpRequestException(PhpRequestException::UNDEFINED);
		}

		$headers = getallheaders();

		if (!is_array($headers)) {
			throw new PhpRequestException(PhpRequestException::FAILURE);
		}

		return $headers;
	}

	/**
	 * Converts the given array describing uploaded files to an array of
	 * uploads.
	 *
	 * @param array $files the array describing uploaded files to convert
	 * @return array the converted array
	 * @see https://php.net/manual/features.file-upload.post-method.php
	 */
	public static function toUploads(array $files)
	{
		$basis = 'error';
		$uploads = array();
		$fields = array('name', 'type', 'size', 'tmp_name', $basis);

		foreach ($files as $key => $file) {

			// Determine whether or not subkeys should be appended to the key.
			$append = is_array($file[$basis]);

			// If the current file is singular, normalize it as an array.
			if (!$append) {
				foreach ($fields as $field) {
					$file[$field] = array($file[$field]);
				}
			}

			$next = self::toUploadsRecursively(
				$key,
				$file['name'],
				$file['type'],
				$file['size'],
				$file['tmp_name'],
				$file['error'],
				$append
			);
			$uploads = array_merge($uploads, $next);
		}

		return $uploads;
	}

	/**
	 * Recursively flatten the given key and arrays describing uploaded files.
	 *
	 * @param string $key the key of the given arrays
	 * @param array $names the names of the uploaded files
	 * @param array $types the types of the uploaded files
	 * @param array $sizes the sizes of the uploaded files
	 * @param array $paths the paths of the uploaded files
	 * @param array $errors the errors of the uploaded files
	 * @param bool $append whether or not the subkey should be appended to the
	 * key
	 * @return array an associative array mapping input names to uploads
	 * @see https://php.net/manual/features.file-upload.post-method.php
	 */
	private static function toUploadsRecursively(
		$key,
		array $names,
		array $types,
		array $sizes,
		array $paths,
		array $errors,
		$append = false
	) {

		$all = array();

		foreach ($errors as $subkey => $error) {

			$full = $key;

			if ($append) {
				$full .= "[{$subkey}]";
			}

			$name = $names[$subkey];
			$type = $types[$subkey];
			$size = $sizes[$subkey];
			$path = $paths[$subkey];

			if (is_array($name)) {
				$all = array_merge(
					$all,
					self::toUploadsRecursively(
						$full,
						$name,
						$type,
						$size,
						$path,
						$error,
						$append
					)
				);
			} else {
				$all[$full] = new Upload($name, $type, $size, $path, $error);
			}
		}

		return $all;
	}

	/**
	 * Constructs a request based on the server API.
	 *
	 * @param string $method the method of the request
	 * @param string $path the path of the request
	 */
	public function __construct($method, $path)
	{
		$this->method = $method;
		$this->path = $path;
	}

	public function method()
	{
		return $this->method;
	}

	public function path()
	{
		return $this->path;
	}

	public function params()
	{
		if (is_null($this->params)) {
			$get = isset($_GET) ? $_GET : array();
			$this->params = new ImmutableArrayObject($get);
		}

		return $this->params;
	}

	public function headers()
	{
		if (is_null($this->headers)) {
			$this->headers = new ImmutableArrayObject(self::toHeaders());
		}

		return $this->headers;
	}

	public function cookies()
	{
		if (is_null($this->cookies)) {
			$cookies = isset($_COOKIE) ? $_COOKIE : array();
			$this->cookies = new ImmutableArrayObject($cookies);
		}

		return $this->cookies;
	}

	public function session()
	{
		if (is_null($this->session)) {
			$this->session = new Session;
		}

		return $this->session;
	}

	public function open()
	{
		$body = fopen(self::BODY, 'rb');

		return is_resource($body) ? $body : null;
	}

	public function data()
	{
		if (is_null($this->data)) {
			$data = isset($_POST) ? $_POST : array();
			$this->data = new ImmutableArrayObject($data);
		}

		return $this->data;
	}

	public function uploads()
	{
		if (is_null($this->uploads)) {
			$files = isset($_FILES) ? $_FILES : array();
			$this->uploads = self::toUploads($files);
		}

		return $this->uploads;
	}

	public function args()
	{
		if (is_null($this->args)) {
			$this->args = new ImmutableArrayObject();
		}

		return $this->args;
	}

	public function token()
	{
		if (is_null($this->token)) {
			$this->token = new EmptyToken();
		}

		return $this->token;
	}

	public function root()
	{
		return $this;
	}
}
