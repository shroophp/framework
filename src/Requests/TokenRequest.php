<?php

namespace ShrooPHP\Framework\Requests;

use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\RequestTrait;
use ShrooPHP\Core\Token;

/**
 * A request being associated with a specific token.
 */
class TokenRequest implements IRequest
{
	use RequestTrait;

	/**
	 * @var \ShrooPHP\Core\Request the request that is having a specific token
	 * associated with it
	 */
	private $request;

	/**
	 * @var \ShrooPHP\Core\Token the token being associated with the request
	 */
	private $token;

	/**
	 * Constructs an association between the given request and the given
	 * token.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to associate with a
	 * specific token
	 * @param \ShrooPHP\Core\Token $token the token to associate with the
	 * request
	 */
	public function __construct(IRequest $request, Token $token)
	{
		$this->request = $request;
		$this->token = $token;
	}

	public function token()
	{
		return $this->token;
	}

	protected function request()
	{
		return $this->request;
	}
}
