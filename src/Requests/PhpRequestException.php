<?php

namespace ShrooPHP\Framework\Requests;

use Exception;
use LogicException;

/**
 * An exception indicating that an error occurred during the retrieval of
 * headers.
 */
class PhpRequestException extends LogicException
{
	/**
	 * The code indicating that the exception relates to the server API.
	 */
	const SAPI = 0b1;

	/**
	 * The code associated with the request method being undefined in the
	 * server API.
	 */
	const SAPI_REQUEST_METHOD = 0b10;

	/**
	 * The code associated with the request URI being undefined in the server
	 * API.
	 */
	const SAPI_REQUEST_URI = 0b100;

	/**
	 * The code associated with `getallheaders()` being undefined.
	 */
	const UNDEFINED = 0b10;

	/**
	 * The code associated with a failure during the retrieval of headers.
	 */
	const FAILURE = 0b100;

	/**
	 * Constructs an exception indicating that an error occurred during the
	 * retrieval of headers.
	 *
	 * @param int $code the exception code (as a value of one the class
	 * constants)
	 * @param Exception $previous the previous exception used for exception
	 * chaining
	 */
	public function __construct($code = 0, Exception $previous = null)
	{
		parent::__construct(self::toMessage($code), $code, $previous);
	}

	/**
	 * Converts the given code to a message.
	 *
	 * @param int $code the code to convert
	 * @return string the converted code
	 */
	private static function toMessage($code)
	{
		return self::SAPI & $code
			? self::toSapiMessage($code)
			: self::toHeadersMessage($code);
	}

	/**
	 * Converts the given code to a message relating to the server API.
	 *
	 * @param int $code the code to convert
	 * @return string the converted code
	 */
	private static function toSapiMessage($code)
	{
		$message = '';
		$verb = 'is';
		$nouns = array();

		if (self::SAPI_REQUEST_METHOD & $code) {
			$nouns[] = '$_SERVER[REQUEST_METHOD]';
		}

		if (self::SAPI_REQUEST_URI & $code) {
			$verb = $nouns ? 'are' : $verb;
			$nouns[] = '$_SERVER[REQUEST_URI]';
		}

		if ($nouns) {
			$message .= implode(' and ', $nouns);
			$message .= " {$verb} undefined";
		}

		return $message;
	}

	/**
	 * Converts the given code to a message relating to headers.
	 *
	 * @param int $code the code to convert
	 * @return string the converted code
	 */
	private static function toHeadersMessage($code)
	{
		$message = '';

		switch ($code) {
			case self::UNDEFINED:
				$message = 'getallheaders() is undefined';
				break;
			case self::FAILURE:
				$message = 'Failed to retrieve headers';
				break;
		}

		return $message;
	}
}
