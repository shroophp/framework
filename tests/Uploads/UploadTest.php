<?php

namespace ShrooPHP\Framework\Tests\Uploads;

use ShrooPHP\Core\Upload as IUpload;
use ShrooPHP\Framework\Uploads\Upload;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Uploads\Upload.
 */
class UploadTest extends TestCase
{
	/**
	 * @var string the currently expected name of the upload
	 */
	private $name;

	/**
	 * @var string the currently expected  of the upload
	 */
	private $type;

	/**
	 * @var int the currently expected size of the upload
	 */
	private $size;

	/**
	 * @var string the currently expected path of the upload
	 */
	private $path;

	/**
	 * @var int the currently expected error associated with the upload
	 */
	private $error;

	/**
	 * Sets up each test by initializing the expected values of the upload.
	 */
	public function setUp()
	{
		$this->name = 'name';
		$this->type = 'text/plain';
		$this->size = 1;
		$this->path = 'php://temp';
		$this->error = UPLOAD_ERR_OK;
	}

	/**
	 * Asserts that the constructor of the upload has expected behavior.
	 */
	public function testConstructor()
	{
		$this->assertUpload();
	}

	/**
	 * Asserts that the setters of the upload have expected behavior.
	 */
	public function testSetters()
	{
		$upload = new Upload('', '', 0, '', 1);
		$this->assertUpload($this->modify($upload));
	}

	/**
	 * Asserts that the given upload matches the current state of the test
	 * case.
	 *
	 * @param \ShrooPHP\Core\Upload $upload the upload to assert
	 */
	private function assertUpload(IUpload $upload = null)
	{
		if (is_null($upload)) {
			$upload = $this->toUpload();
		}

		$this->assertEquals($this->name, $upload->name());
		$this->assertEquals($this->type, $upload->type());
		$this->assertEquals($this->size, $upload->size());
		$this->assertEquals($this->path, $upload->path());
		$this->assertEquals($this->error, $upload->error());
	}

	/**
	 * Modifies the given upload according to the state of the test case.
	 *
	 * @param \ShrooPHP\Framework\Uploads\Upload $upload the upload to modify
	 * @return \ShrooPHP\Framework\Uploads\Upload the modified upload
	 */
	private function modify(Upload $upload)
	{
		$this->assertSame($upload, $upload->setName($this->name));
		$this->assertSame($upload, $upload->setType($this->type));
		$this->assertSame($upload, $upload->setSize($this->size));
		$this->assertSame($upload, $upload->setPath($this->path));
		$this->assertSame($upload, $upload->setError($this->error));

		return $upload;
	}

	/**
	 * Converts the test case to an upload.
	 *
	 * @return \ShrooPHP\Framework\Uploads\Upload the generated upload
	 */
	private function toUpload()
	{
		return new Upload(
			$this->name,
			$this->type,
			$this->size,
			$this->path,
			$this->error
		);
	}
}

