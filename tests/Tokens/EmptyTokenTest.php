<?php

namespace ShrooPHP\Framework\Tests\Tokens;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Framework\Tokens\EmptyToken;

/**
 * A test case for \ShrooPHP\Framework\Tokens\EmptyToken.
 */
class EmptyTokenTest extends TestCase
{
	/**
	 * Asserts that an empty token has expected behavior.
	 */
	public function testEmptyToken()
	{
		$token = new EmptyToken();
		$this->assertEquals('', $token->expected());
		$this->assertFalse($token->valid());
	}
}

