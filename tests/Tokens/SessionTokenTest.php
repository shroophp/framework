<?php

namespace ShrooPHP\Framework\Tests\Tokens;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\ArrayObject;
use ShrooPHP\Core\Readable;
use ShrooPHP\Core\Session;
use ShrooPHP\Core\Token\Generator;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Tests\Token\Generators\RandomGeneratorTest;
use ShrooPHP\Framework\Tokens\SessionToken;

/**
 * A test case for \ShrooPHP\Framework\Tokens\SessionToken.
 */
class SessionTokenTest extends TestCase implements Generator, Session
{
	/**
	 * The number of bytes being used by the session token to generate tokens.
	 */
	const BYTES = Application::BYTES;

	/**
	 * The token returned by the dummy generator during tests.
	 */
	const TOKEN = __CLASS__;

	/**
	 * @var \ShrooPHP\Core\ArrayObject the array object representing the
	 * session during the current test
	 */
	private $session;

	public function generate()
	{
		return self::TOKEN;
	}

	public function get($key, $default = null)
	{
		return $this->session->get($key, $default);
	}

	public function set($key, $value)
	{
		$this->session->set($key, $value);
	}

	public function commit()
	{
		// Do nothing.
	}

	/**
	 * Sets up each test by initializing the array object representing the
	 * session.
	 */
	public function setUp()
	{
		$this->session = new ArrayObject;
	}

	/**
	 * Asserts that the constructor of the session token has expected behavior.
	 */
	public function testConstructor()
	{
		$token = new SessionToken($this, self::TOKEN, $this);

		$this->assertEquals(self::TOKEN, $token->expected());
		$this->assertSession($this, self::TOKEN);
		$this->assertTrue($token->valid());
	}

	/**
	 * Asserts that the given readable object holds the given token.
	 *
	 * @param \ShrooPHP\Core\Readable $session the readable object to evaluate
	 * @param string $token the token to assert
	 */
	private function assertSession(Readable $session, $token)
	{
		$this->assertEquals($token, $this->session->get(SessionToken::KEY));
	}
}
