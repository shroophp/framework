<?php

namespace ShrooPHP\Framework\Tests\Sessions;

use ShrooPHP\Framework\Sessions\Session;
use ShrooPHP\Framework\Sessions\SessionException;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Sessions\Session.
 */
class SessionTest extends TestCase
{
	/**
	 * The message to print when failing to assert that sessions are disabled.
	 */
	const MESSAGE = 'Expected sessions to be disabled.';

	/**
	 * Asserts that various interactions with the session have expected
	 * behavior.
	 *
	 * @runInSeparateProcess
	 */
	public function testSession()
	{
		$first = 'key1';
		$second = 'key2';
		$init = 'init';
		$reset = 'reset';
		$unset = 'unset';
		$default = 'default';
		$final = array($first => $reset, $second => $init);
		$session = new Session;

		// Ensure that sessions are enabled.
		ini_set('session.save_handler', 'files');

		// Assert that the session is yet to be started.
		$this->assertStatus(false);

		// Assert that values can be set.
		$session->set($first, $init);
		$this->assertStatus(true);

		// Assert that set values can be retrieved (even before committing).
		$this->assertEquals($init, $session->get($first, $default));

		// Assert that the session global variable has been modified.
		$this->assertEquals(array($first => $init), $_SESSION);

		// Assert that values can be overwritten.
		$session->set($first, $reset);
		$this->assertStatus(true);

		// Assert that the session global variable has been modified.
		$this->assertEquals(array($first => $reset), $_SESSION);

		// Assert that unset values default to NULL.
		$this->assertNull($session->get($unset));
		$this->assertStatus(true);

		// Assert that the default value of unset values can be overridden.
		$this->assertEquals($default, $session->get($unset, $default));
		$this->assertStatus(true);

		// Assert that a commit closes the session.
		$session->commit();
		$this->assertStatus(false);

		// Assert that value retrieval maintains an inactive session.
		$this->assertEquals($reset, $session->get($first, $default));
		$this->assertStatus(false);

		// Assert that setting avalue maintains an inactive session.
		$session->set($second, $init);
		$this->assertStatus(false);
		$this->assertEquals($init, $session->get($second, $default));
		$this->assertStatus(false);

		// Assert that another commit closes the session once again.
		$session->commit();
		$this->assertStatus(false);

		// Assert the final state of the global variable.
		$this->assertEquals($final, $_SESSION);

	}

	/**
	 * Asserts that the expected exception is thrown when writing to the
	 * session while sessions are disabled.
	 */
	public function testDisabled()
	{
		$exception = null;
		$session = new Session;

		try {
			@$session->set('', '');
		} catch (SessionException $exception) {
			$code = $exception->getCode();
			$this->assertEquals(SessionException::DISABLED, $code);
		}

		$this->assertNotNull($exception, self::MESSAGE);
	}

	/**
	 * Asserts that the expected exception is thrown when reading from the
	 * session while sessions are disabled.
	 */
	public function testCannotStart()
	{
		$exception = null;
		$session = new Session;

		try {
			@$session->get('');
		} catch (SessionException $exception) {
			$code = $exception->getCode();
			$this->assertEquals(SessionException::CANNOT_START, $code);
		}

		$this->assertNotNull($exception, self::MESSAGE);
	}

	/**
	 * Asserts that the session is in the given state.
	 *
	 * @param bool $active TRUE to assert the status as active, or FALSE to
	 * assert the status as inactive
	 */
	private function assertStatus($active)
	{
		$flag = $active ? PHP_SESSION_ACTIVE : PHP_SESSION_NONE;
		$this->assertEquals($flag, session_status());
	}
}
