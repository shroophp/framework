<?php

namespace ShrooPHP\Framework\Tests\Application\Modifiers;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Core\Request\Response as IResponse;
use ShrooPHP\Core\Request\Response\Presenter;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\Framework\Application\Modifiers\HandlerAdapter;

/**
 * A test case for \ShrooPHP\Framework\Application\Modifiers\HandlerAdapter.
 */
class HandlerAdapterTest extends TestCase implements Handler, Presenter
{
	/**
	 * @var bool the current return value of the handler
	 */
	private $returns;

	/**
	 * @var int the number of times that the handler has been called during the
	 * current test
	 */
	private $handled;

	/**
	 * @var int the number of times that the presenter has been called during
	 * the current test
	 */
	private $presented;

	/**
	 * Sets up each test by initializing the number of times that the handler
	 * has been called.
	 */
	public function setUp()
	{
		$this->handled = 0;
		$this->presented = 0;
	}

	public function handle(IRequest $request)
	{
		$this->handled++;

		return $this->returns;
	}

	public function present(IResponse $response)
	{
		$this->presented++;
	}

	/**
	 * Responds to the given request.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to respond to
	 * @return \ShrooPHP\Core\Request\Response the response to the request
	 */
	public function respond(IRequest $request)
	{
		$this->handled++;

		return new Response;
	}

	/**
	 * Asserts that the constructor of the adapter has expected behavior when
	 * given a handler that handles the request.
	 */
	public function testConstructorWithHandled()
	{
		$adapter = new HandlerAdapter($this);
		$this->assertHandlerAdapter($adapter, true);
	}

	/**
	 * Asserts that the constructor of the adapter has expected behavior when
	 * given a handler that does not handle the request.
	 */
	public function testConstructorWithUnhandled()
	{
		$adapter = new HandlerAdapter($this);
		$this->assertHandlerAdapter($adapter, false);
	}

	/**
	 * Asserts that the create method of the adapter has expected behavior when
	 * given a handler that handles the request.
	 */
	public function testCreateWithHandled()
	{
		$adapter = HandlerAdapter::create($this, $this);
		$this->assertHandlerAdapter($adapter, true);
	}

	/**
	 * Asserts that the create method of the adapter has expected behavior when
	 * given a callback that handles the request.
	 */
	public function testCreateWithHandledCallback()
	{
		$adapter = HandlerAdapter::create(array($this, 'handle'), $this);
		$this->assertHandlerAdapter($adapter, true);
	}

	/**
	 * Asserts that the create method of the adapter has expected behavior when
	 * given a handler that does not handle the request.
	 */
	public function testCreateWithUnhandled()
	{
		$adapter = HandlerAdapter::create($this, $this);
		$this->assertHandlerAdapter($adapter, false);
	}

	/**
	 * Asserts that the create method of the adapter has expected behavior when
	 * given a callback that does not handle the request.
	 */
	public function testCreateWithUnhandledCallback()
	{
		$adapter = HandlerAdapter::create(array($this, 'handle'), $this);
		$this->assertHandlerAdapter($adapter, false);
	}

	/**
	 * Asserts that the create method of the adapter has expected behavior when
	 * given a callback that returns a response.
	 */
	public function testCreateWithRespondedCallback()
	{
		$adapter = HandlerAdapter::create(array($this, 'respond'), $this);
		$this->assertHandlerAdapter($adapter, true, true);
	}

	/**
	 * Asserts the given adapter.
	 *
	 * @param \ShrooPHP\Framework\Application\Modifier\HandlerAdapter $adapter
	 * the adapter to assert
	 * @param bool $returns the expected return value of the associated handler
	 * @param bool $present whether or not the presenter is expected to be
	 * called
	 */
	private function assertHandlerAdapter(
		HandlerAdapter $adapter,
		$returns,
		$present = false
	) {
		$handled = $this->handled;
		$presented = $this->presented;
		$app = new Application;
		$this->returns = $returns;

		if ($present) {
			$presented += 1;
		}

		$app->path('', $adapter)->handle(new Request('', ''));
		$this->assertEquals($handled + 1, $this->handled);
		$this->assertEquals($presented, $this->presented);
	}
}
