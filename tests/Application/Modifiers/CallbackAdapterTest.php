<?php

namespace ShrooPHP\Framework\Tests\Application\Modifiers;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Framework\Application as IApplication;
use ShrooPHP\Framework\Application\Modifier;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Application\Modifiers\CallbackAdapter;

/**
 * A test case for \ShrooPHP\Framework\Application\Modifiers\CallbackAdapter.
 */
class CallbackAdapterTest extends TestCase implements Modifier
{
	/**
	 * @var \ShrooPHP\Framework\Application the currently expected application
	 */
	private $app;

	/**
	 * @var int the number of times an application has been modified during the
	 * current test
	 */
	private $modified;

	/**
	 * Asserts that the given application is expected.
	 *
	 * @param \ShrooPHP\Framework\Application $app the application to assert
	 */
	public function modify(IApplication $app)
	{
		$this->assertSame($this->app, $app);
		$this->modified++;
	}

	/**
	 * Does nothing.
	 */
	public function doNothing()
	{
		// Do nothing.
	}

	/**
	 * Sets up each test by initializing the number of modifications.
	 */
	public function setUp()
	{
		$this->modified = 0;
		$this->app = new Application;
	}

	/**
	 * Asserts that the constructor of the adapter has expected behavior.
	 */
	public function testConstructor()
	{
		$this->assertCallbackAdapter(new CallbackAdapter($this->toCallback()));
	}

	/**
	 * Generates a callback for the current test.
	 *
	 * @return callable the generated callback
	 */
	private function toCallback()
	{
		return array($this, 'modify');
	}

	/**
	 * Asserts the given adapter.
	 *
	 * @param CallbackAdapter $adapter the adapter to assert
	 */
	private function assertCallbackAdapter(CallbackAdapter $adapter)
	{
		$modified = $this->modified;

		$this->modify($this->app);

		$this->assertEquals($modified + 1, $this->modified);
	}

}
