<?php

namespace ShrooPHP\Framework\Tests\Request\Handlers;

use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Request\Handlers\AggregateHandler;
use ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandlerTest\Handler;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandler.
 */
class AggregateHandlerTest extends TestCase
{
	/**
	 * @var \ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandler the
	 * handler being tested
	 */
	private $adapter;

	/**
	 * @var \ShrooPHP\Framework\Requests\Request the request currently being
	 * used in tests.
	 */
	private $request;

	/**
	 * @var array signatures in order of when they were signed
	 */
	private $trace;

	/**
	 * Push a signature onto the trace.
	 *
	 * @param string $signature the signature to push onto the trace
	 */
	public function sign($signature)
	{
		$this->trace[] = $signature;
	}

	/**
	 * Sets up each test by initializing the adapter, the request, and the
	 * trace of signatures.
	 */
	public function setUp()
	{
		$this->adapter = new AggregateHandler;
		$this->request = new Request('', '');
		$this->trace = array();
	}

	/**
	 * Asserts that requests are handled as expected.
	 */
	public function testHandle()
	{
		$this->adapter->push($this->toHandler('first', false));
		$this->adapter->push($this->toHandler('second', false));
		$this->adapter->push($this->toHandler('third', true));
		$this->adapter->push($this->toHandler('fourth', true));
		$this->adapter->handle($this->request);
		$this->assertEquals(array('first', 'second', 'third'), $this->trace);
	}

	/**
	 * Generates a stub handler.
	 *
	 * @param type $signature the signature that the handler will sign with
	 * when triggered
	 * @param bool $returns the value that the handler will return when
	 * triggered
	 * @return \ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandlerTest\Handler
	 * the generated handler
	 */
	private function toHandler($signature, $returns)
	{
		return new Handler($this, $signature, $returns);
	}
}
