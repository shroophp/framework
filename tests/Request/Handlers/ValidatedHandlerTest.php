<?php

namespace ShrooPHP\Framework\Tests\Request\Handlers;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Core\Request\Validator;
use ShrooPHP\Framework\Request\Handlers\ValidatedHandler;
use ShrooPHP\Framework\Tests\Requests\RequestTest;

/**
 * A test case for \ShrooPHP\Framework\Request\Handlers\ValidatedHandler.
 */
class ValidatedHandlerTest extends TestCase implements Handler, Validator
{
	/**
	 * @var int the number of times that a request has been handled during the
	 * current test
	 */
	private $handled;

	/**
	 * @var bool the current validity of the validator
	 */
	private $validity;

	/**
	 * Sets up each test by initializing the number of times a request has been
	 * handled and the validity of the validator.
	 */
	public function setUp()
	{
		$this->handled = 0;
		$this->validity = false;
	}

	public function handle(Request $request)
	{
		$this->handled++;
		return true;
	}

	public function validate(Request $request)
	{
		return $this->validity;
	}

	/**
	 * Asserts that the constructor of the handler has expected behavior when
	 * given a valid request.
	 */
	public function testValidViaConstructor()
	{
		$this->assertValidatedHandler($this->toValidatedHandler(), true);
	}

	/**
	 * Asserts that the constructor of the handler has expected behavior when
	 * given an invalid request.
	 */
	public function testInvalidViaConstructor()
	{
		$this->assertValidatedHandler($this->toValidatedHandler(), false);
	}

	/**
	 * Converts the current test case to a validated handler.
	 *
	 * @return \ShrooPHP\Framework\Request\Handlers\ValidatedHandler the
	 * converted test case
	 */
	private function toValidatedHandler()
	{
		return new ValidatedHandler($this, $this);
	}

	/**
	 * Asserts that the given handler has expected behavior when passed a
	 * request with the given validity.
	 *
	 * @param \ShrooPHP\Framework\Request\Handlers\ValidatedHandler $handler
	 * the handler to assert
	 * @param bool $valid the validity of the request
	 */
	private function assertValidatedHandler(ValidatedHandler $handler, $valid)
	{
		$handled = $this->handled;
		$this->validity = $valid;

		$this->assertEquals($valid, $handler->handle(RequestTest::create()));

		if ($valid) {
			$handled++;
		}

		$this->assertEquals($handled, $this->handled);
	}
}

