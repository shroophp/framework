<?php

namespace ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandlerTest;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler as IHandler;
use ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandlerTest;

/**
 * A stub handler for
 * \ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandlerTest.
 */
class Handler implements IHandler
{
	/**
	 * @var ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandlerTest the
	 * test case being signed whenever the handler is triggered
	 */
	private $test;

	/**
	 * @var string the signature to sign with whenever the handler is triggered
	 */
	private $signature;

	/**
	 * @var bool the value being returned whenever the handler is triggered
	 */
	private $returns;

	/**
	 * Constructs a stub handler.
	 *
	 * @param \ShrooPHP\Framework\Tests\Request\Handlers\AggregateHandlerTest
	 * $test the test to sign whenever the handler is triggered
	 * @param string $signature the signature to sign with whenever the handler
	 * is triggered
	 * @param bool $returns the value to return whenever the handler is
	 * triggered
	 */
	public function __construct(AggregateHandlerTest $test, $signature, $returns)
	{
		$this->test = $test;
		$this->signature = $signature;
		$this->returns = $returns;
	}

	public function handle(Request $request)
	{
		$this->test->sign($this->signature);
		return $this->returns;
	}
}
