<?php

namespace ShrooPHP\Framework\Tests\Request\Handlers;

use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Response;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Framework\Request\Handlers\CallbackAdapter;
use ShrooPHP\Framework\Tests\Request\Handlers\AdapterTestCase;

/**
 * A test case for \ShrooPHP\Framework\Request\Handlers\CallbackAdapter.
 */
class CallbackAdapterTest extends AdapterTestCase implements Handler
{
	/**
	 * @var mixed the value currently being returned by the handler
	 */
	private $returns;

	public function handle(Request $request)
	{
		$this->assertSame($this->request(), $request);
		$this->handled(1);

		return $this->returns;
	}

	/**
	 * Asserts that the callback adapter has expected behavior when the
	 * callback handles the request.
	 */
	public function testHandlingCallback()
	{
		$this->assertCallbackAdapter(false);
	}

	/**
	 * Asserts that the callback adapter has expected behavior when the
	 * callback does not handle the request.
	 */
	public function testUnhandlingCallback()
	{
		$this->assertCallbackAdapter(true);
	}

	/**
	 * Asserts that the callback adapter has expected behavior when the
	 * callback returns a response.
	 */
	public function testRespondingCallback()
	{
		$this->assertCallbackAdapter($this->response());
	}

	/**
	 * Asserts that the callback adapter has expected behavior when the adapted
	 * returns the given value.
	 *
	 * @param \ShrooPHP\Core\Request\Response|bool|null $returns the return
	 * value of the callback
	 */
	private function assertCallbackAdapter($returns)
	{
		$handled = $this->handled();
		$presented = $this->presented();
		$adapter = new CallbackAdapter(array($this, 'handle'), $this);
		$this->returns = $returns;

		if ($returns instanceof Response) {
			$presented += 1;
		}

		$this->assertEquals(!!$returns, $adapter->handle($this->request()));
		$this->assertEquals($handled + 1, $this->handled());
		$this->assertEquals($presented, $this->presented());
	}
}
