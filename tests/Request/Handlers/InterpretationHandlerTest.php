<?php

namespace ShrooPHP\Framework\Tests\Request\Handlers;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler;
use ShrooPHP\Core\Pattern\Interpretation;
use ShrooPHP\Framework\Request\Handlers\InterpretationHandler;
use ShrooPHP\Framework\Tests\Requests\RequestTest;

/**
 * A test case for \ShrooPHP\Framework\Request\Handlers\InterpretationHandler.
 */
class InterpretationHandlerTest extends TestCase implements Handler, Interpretation
{
	/**
	 * @var \ShrooPHP\Core\Request the currently expected request
	 */
	private $request;

	/**
	 * @var array $args the current return value of the interpretation (and
	 * hence the currently expected arguments of the
	 */
	private $args;

	/**
	 * @var bool the current return value of the handler
	 */
	private $handle;

	/**
	 * @var bool whether or not the
	 */
	private $handled;


	public function handle(Request $request)
	{
		$expected = is_null($this->args) ? array() : $this->args;

		RequestTest::assert($this, $this->request, $request, array(
			RequestTest::OVERRIDE_ARGS => $request->args(),
		));

		$this->assertEquals($expected, $request->args()->getArrayCopy());

		$this->handled = true;

		return $this->handle;
	}

	public function match($subject)
	{
		return $this->args;
	}

	/**
	 * Asserts that the interpretation handler will not handle the request if
	 * the path does not match.
	 */
	public function testUnmatched()
	{
		$this->assertInterpretationHandler(null, false, false);
	}

	/**
	 * Asserts that the interpretation handler will not handle the request if
	 * the path does not match, despite the fact that the handler would handle
	 * the request if it were called.
	 */
	public function testUnmatchedButWouldHandle()
	{
		$this->assertInterpretationHandler(null, true, false);
	}

	/**
	 * Asserts that the interpretation handler will handle the request if the
	 * path matches, but will flag the request as not being handled if the
	 * handler does not handle it.
	 */
	public function testMatchedButUnhandled()
	{
		$this->assertInterpretationHandler(array(), false, true);
	}


	/**
	 * Asserts that the interpretation handler will handle the request if the
	 * path matches, but will flag the request as not being handled if the
	 * handler does not handle it (despite specific arguments being associated
	 * with the request).
	 */
	public function testMatchedButUnhandledWithArgs()
	{
		$this->assertInterpretationHandler(array('arg' => 'arg'), false, true);
	}

	/**
	 * Asserts that the interpretation handler will handle the request if the
	 * path matches, and will flag the request has handled if the handler does
	 * so.
	 */
	public function testMatchedAndHandled()
	{
		$this->assertInterpretationHandler(array(), true, true);
	}

	/**
	 * Asserts that the interpretation handler will handle the request if the
	 * path matches, will associate specific arguments with the request, and
	 * will flag the request as handled if the handler does so.
	 */
	public function testMatchedAndHandledWithArgs()
	{
		$this->assertInterpretationHandler(array('arg' => 'arg'), true, true);
	}

	/**
	 * Asserts that the interpretation handler will associate the given
	 * arguments with the request (if any) when the handler has the specified
	 * behavior.
	 *
	 * @param array|null $args the arguments to associate with the request (if
	 * any)
	 * @param bool $handle the return value of the handler
	 * @param bool $handled whether or not the handler will be called
	 */
	private function assertInterpretationHandler(
		array $args = null,
		$handle = false,
		$handled = false
	) {

		$expected = $handle && $handled;
		$handler = new InterpretationHandler($this, $this);

		$this->request = RequestTest::create();
		$this->handled = false;
		$this->args = $args;
		$this->handle = $handle;

		$this->assertEquals($expected, $handler->handle($this->request));
		$this->assertEquals($handled, $this->handled);
	}
}
