<?php

namespace ShrooPHP\Framework\Tests\Request\Handlers;

use ShrooPHP\Core\Pattern\Interpreter as IInterpreter;
use ShrooPHP\Core\Pattern\Interpretation;
use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\Request\Response as IResponse;
use ShrooPHP\Core\Request\Response\Presenter;
use ShrooPHP\Core\Runnable;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\Pattern\Interpreter;
use PHPUnit\Framework\TestCase;

/**
 * A test case for request handler adapters.
 */
abstract class AdapterTestCase extends TestCase implements IInterpreter, Interpretation, Presenter
{

	/**
	 * @var \ShrooPHP\Core\Pattern\Interpreter the interpreter
	 * currently being used to interpret patterns
	 */
	private $interpreter;

	/**
	 * @var int the number of times that a subject has been matched by the test
	 * case during the current test
	 */
	private $matched;

	/**
	 * @var \ShrooPHP\Framework\Request the request currently being used in
	 * tests
	 */
	private $request;

	/**
	 * @var array the expected arguments when the request is handled
	 */
	private $args;

	/**
	 * @var int the number of times that a request has been handled by the test
	 * case during the current test
	 */
	private $handled;

	/**
	 * @var \ShrooPHP\Core\Request\Response the currently expected response
	 */
	private $response;

	/**
	 * @var string|null the expected content type of the response (if any)
	 */
	private $type;

	/**
	 * @var string|null the expected content type of the response (if any)
	 */
	private $content;

	/**
	 * @var int the number of times that a response has been presented by the
	 * test case during the current test
	 */
	private $presented;

	/**
	 * Sets up each test by initializing the interpreter, the number of matched
	 * patterns, the request, the expected arguments, the expected response,
	 * and the number of presented responses.
	 */
	public function setUp()
	{
		$this->interpreter = new Interpreter;
		$this->matched = 0;
		$this->request(new Request('', ''));
		$this->args(array());
		$this->response(new Response);
		$this->presented = 0;
		$this->handled = 0;
	}

	public function interpret($pattern)
	{
		return $this->interpreter->interpret($pattern);
	}

	public function match($subject)
	{
		$this->matched++;
		return array();
	}

	public function present(IResponse $response)
	{
		$headers = null;
		$bypass = false;
		$found = false;

		if (!is_null($this->type)) {

			$headers = $response->headers();
			$this->assertNotNull($headers);
			$bypass = true;
		}

		if (!is_null($headers)) {

			foreach ($headers as $header => $value) {
				switch (strtolower($header)) {
					case 'content-type':
						$this->assertEquals($this->type, $value);
						$found = true;
						break;
				}
			}

			$this->assertTrue($found);
		}

		if (!is_null($this->content)) {

			$content = $response->content();

			if (!is_null($content)) {
				$content = $this->toContent($content);
			}

			$this->assertEquals($this->content, $content);

			$bypass = true;
		}

		if (!$bypass) {
			$this->assertSame($this->response(), $response);
		}

		$this->presented++;
	}

	/**
	 * Gets the number of patterns matched during the current test.
	 *
	 * @return int the number of patterns matched
	 */
	protected function matched()
	{
		return $this->matched;
	}

	/**
	 * Gets the current request.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to set as the current
	 * (if specified)
	 * @return \ShrooPHP\Core\Request $request the current request
	 */
	protected function request(IRequest $request = null)
	{
		if (!is_null($request)) {
			$this->request = $request;
		}

		return $this->request;
	}

	/**
	 * Gets the currently expected arguments.
	 *
	 * @param array $args the arguments to set as the current (if specified)
	 * @return array the currently expected arguments
	 */
	protected function args(array $args = null)
	{
		if (!is_null($args)) {
			$this->args = $args;
		}

		return $this->args;
	}

	/**
	 * Gets the number of requests handled during the current test.
	 *
	 * @param int|null $add the amount to increase the number of handled
	 * requests by (if specified)
	 * @return int the number of requests handled during the current test
	 */
	protected function handled($add = null)
	{
		if (!is_null($add)) {
			$this->handled += $add;
		}

		return $this->handled;
	}

	/**
	 * Gets the currently expected response.
	 *
	 * @param \ShrooPHP\Core\Request\Response $response the response to set as
	 * the current (if specified)
	 * @return \ShrooPHP\Core\Request\Response the currently expected response
	 */
	protected function response(Response $response = null)
	{
		if (!is_null($response)) {
			$this->response = $response;
		}

		return $this->response;
	}

	/**
	 * Sets the expected content type of the response (if any).
	 *
	 * If specified, checks for the specific expected response will be bypassed.
	 *
	 * @param string|null $type the expected content type
	 */
	protected function type($type)
	{
		$this->type = $type;
	}

	/**
	 * Sets the expected content of the response (if any).
	 *
	 * If specified, checks for the specific expected response will be bypassed.
	 *
	 * @param string|null $type the expected content type
	 */
	protected function content($content)
	{
		$this->content = $content;
	}

	/**
	 * Gets the number of responses presented during the current test.
	 *
	 * @return int the number of responses presented during the current test
	 */
	protected function presented()
	{
		return $this->presented;
	}

	/**
	 * Captures the output of the given runnable as a string.
	 *
	 * @param \ShrooPHP\Core\Runnable $content the content to capture
	 * @return string the captured content
	 */
	private function toContent(Runnable $content)
	{
		ob_start();
		$content->run();
		return ob_get_clean();
	}

}
