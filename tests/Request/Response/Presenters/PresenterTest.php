<?php

namespace ShrooPHP\Framework\Tests\Request\Response\Presenters;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Framework\Request\Response\Presenters\Presenter;
use ShrooPHP\Framework\Request\Responses\Response;

/**
 * A test case for \ShrooPHP\Framework\Request\Response\Presenters\Presenter.
 */
class PresenterTest extends TestCase
{
	/**
	 * Asserts that the presenter has expected behavior.
	 *
	 * @runInSeparateProcess
	 */
	public function testPresenter()
	{
		$expected = array();
		$code = Response::HTTP_I_AM_A_TEAPOT;
		$type = 'text/plain';
		$content = 'Hello, world!';
		$headers = array(
			'Content-Type' => $type,
			'X-Powered-By' => "Mum's Sunday roast.",
		);
		$presenter = new Presenter;
		$response = Response::string($content, $type);

		$response->setCode($code);

		foreach (array_slice($headers, 1) as $header => $value) {
			$response->addHeader($header, $value);
		}

		foreach ($headers as $header => $value) {
			$expected[] = "{$header}: {$value}";
		}

		ob_start();
		$presenter->present($response);
		$this->assertEquals($content, ob_get_clean());
		$this->assertEquals($code, http_response_code());
		$this->assertEquals($expected, $presenter->sent());
	}
}
