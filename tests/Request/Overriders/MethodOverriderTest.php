<?php

namespace ShrooPHP\Framework\Tests\Request\Overriders;

use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Framework\Request\Overriders\MethodOverrider;
use ShrooPHP\Framework\Tests\Requests\RequestTest;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Request\Overriders\MethodOverrider.
 */
class MethodOverriderTest extends TestCase
{
	/**
	 * Asserts that the constructor of the overrider has expected behavior.
	 */
	public function testConstructor()
	{
		$key = __CLASS__;
		$this->assertOverrider(new MethodOverrider($key), $key);
	}

	/**
	 * Asserts the behavior of the given overrider using the given key.
	 *
	 * @param \ShrooPHP\Framework\Request\Overriders\MethodOverrider $overrider
	 * the overrider to assert
	 * @param scalar $key the key being associated with the method
	 */
	public function assertOverrider(MethodOverrider $overrider, $key)
	{
		$data = array($key => __CLASS__);
		$request = RequestTest::create();

		$request->setData(new ImmutableArrayObject($data));
		$overridden = $overrider->override($request);

		RequestTest::assert($this, $request, $overridden, array(
			RequestTest::OVERRIDE_METHOD => __CLASS__,
		));
	}
}

