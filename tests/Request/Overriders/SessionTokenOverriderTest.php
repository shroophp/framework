<?php

namespace ShrooPHP\Framework\Tests\Request\Overriders;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\ArrayObject;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Session;
use ShrooPHP\Core\Token\Generator;
use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Tests\Requests\RequestTest;
use ShrooPHP\Framework\Tests\Token\Generators\RandomGeneratorTest;
use ShrooPHP\Framework\Tokens\SessionToken;
use ShrooPHP\Framework\Request\Overriders\SessionTokenOverrider;

/**
 * A test case for \ShrooPHP\Framework\Request\Overriders\SessionTokenOverrider.
 */
class SessionTokenOverriderTest extends TestCase implements Generator, Session
{
	/**
	 * The data key being used during tests.
	 */
	const KEY = __CLASS__;

	/**
	 * @var \ShrooPHP\Core\ArrayObject the array object being used to represent
	 * the session
	 */
	private $session;

	public function generate()
	{
		return __METHOD__;
	}

	public function get($key, $default = null)
	{
		return $this->session->get($key, $default);
	}

	public function set($key, $value)
	{
		$this->session->set($key, $value);
	}

	public function commit()
	{
		// Do nothing.
	}

	/**
	 * Sets up each test by initializing the array object representing the
	 * session.
	 */
	public function setUp()
	{
		$this->session = new ArrayObject;
	}

	/**
	 * Asserts that the constructor of the session token overrider has expected
	 * behaviour.
	 */
	public function testConstructor()
	{
		$overrider = new SessionTokenOverrider(self::KEY, $this);
		$this->assertSessionTokenOverrider($overrider, $this->generate());
	}

	/**
	 * Converts the test case to a request.
	 *
	 * @return \ShrooPHP\Framework\Requests\Request the converted test case
	 */
	private function toRequest()
	{
		$request = RequestTest::create();
		$request->setSession($this);

		return $request;
	}

	/**
	 * Asserts that the given session token overrider has expected behavior.
	 *
	 * @param \ShrooPHP\Framework\Request\Overriders\SessionTokenOverrider
	 * $overrider the session token overrider to assert
	 * @param string|null $token the generated token value to assert (or NULL
	 * if the generated token will be random)
	 */
	private function assertSessionTokenOverrider(
		SessionTokenOverrider $overrider,
		$token = null
	) {

		$request = $this->toRequest();
		$expected = $this->assertRequest(
			$request,
			$overrider->override($request),
			false,
			$token
		);

		$request->setData(new ImmutableArrayObject(array(
			self::KEY => $expected,
		)));

		$this->assertRequest(
			$request,
			$overrider->override($request),
			true,
			$token
		);
	}

	/**
	 * Asserts that the given requests have deep equality.
	 *
	 * @param \ShrooPHP\Core\Request $expected the expected state of the
	 * request
	 * @param \ShrooPHP\Core\Request $actual the actual state of the request
	 * @param bool $valid the expected validity of the token
	 * @param string|null $token the generated token value to assert (or NULL
	 * if the generated token will be random)
	 * @return type
	 */
	private function assertRequest(
		Request $expected,
		Request $actual,
		$valid = false,
		$token = null
	) {
		$bytes = Application::BYTES;
		$overidden = $actual->token();
		$generated = $overidden->expected();

		RequestTest::assert($this, $expected, $actual, array(
			RequestTest::OVERRIDE_TOKEN => $overidden,
		));

		if (is_null($token)) {
			RandomGeneratorTest::assert($this, $generated, $bytes);
		} else {
			$this->assertEquals($token, $generated);
		}

		$this->assertEquals($valid, $overidden->valid());

		return $generated;
	}
}
