<?php

namespace ShrooPHP\Framework\Tests\Request\Overriders;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Overrider;
use ShrooPHP\Framework\Request\Overriders\CallbackAdapter;
use ShrooPHP\Framework\Tests\Requests\RequestTest;

/**
 * A test case for \ShrooPHP\Framework\Request\Overriders\CallbackAdapter.
 */
class CallbackAdapterTest extends TestCase implements Overrider
{

	public function override(Request $request)
	{
		return $request;
	}

	/**
	 * Asserts that the constructor of the adapter has expected behavior.
	 */
	public function testConstructor()
	{
		$this->assertCallbackAdapter(new CallbackAdapter($this->toCallback()));
	}

	/**
	 * Generates a callback for the current test.
	 *
	 * @return callable the generated callback
	 */
	private function toCallback()
	{
		return array($this, 'override');
	}

	/**
	 * Generates a request for the current test.
	 *
	 * @return \ShrooPHP\Core\Request the generated request
	 */
	private function toRequest()
	{
		return RequestTest::create();
	}

	/**
	 * Asserts the given adapter.
	 *
	 * @param CallbackAdapter $adapter the adapter to assert
	 */
	private function assertCallbackAdapter(CallbackAdapter $adapter)
	{
		$request = $this->toRequest();
		$expected = $this->override($request);

		$this->assertSame($expected, $adapter->override($request));
	}
}
