<?php

namespace ShrooPHP\Framework\Tests\Request\Validators;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Token;
use ShrooPHP\Framework\Request\Validators\TokenValidator;
use ShrooPHP\Framework\Tests\Requests\RequestTest;
use ShrooPHP\Framework\Tokens\EmptyToken;

/**
 * A test case for \ShrooPHP\Framework\Tests\Request\Validators\TokenValidator.
 */
class TokenValidatorTest extends TestCase implements Token
{
	public function expected()
	{
		return __METHOD__;
	}

	public function valid()
	{
		return true;
	}

	/**
	 * Asserts that a request with a valid token is determined as being
	 * valid.
	 */
	public function testValid()
	{
		$this->assertTokenValidator($this, true);
	}

	/**
	 * Asserts that a request with an invalid token is determined as
	 * being invalid.
	 */
	public function testInvalid()
	{
		$this->assertTokenValidator(new EmptyToken, false);
	}

	/**
	 * Asserts that a request with the given token is determined as
	 * being the given validity.
	 *
	 * @param \ShrooPHP\Core\Token the token to assert
	 * @param bool $validity whether or not the token is valid
	 */
	private function assertTokenValidator(Token $token, $validity)
	{
		$validator = new TokenValidator();
		$request = RequestTest::create();

		$request->setToken($token);

		$this->assertEquals($validity, $validator->validate($request));
	}
}

