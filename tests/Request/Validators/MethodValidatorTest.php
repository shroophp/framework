<?php

namespace ShrooPHP\Framework\Tests\Request\Validators;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Framework\Request\Validators\MethodValidator;
use ShrooPHP\Framework\Requests\MethodRequest;
use ShrooPHP\Framework\Tests\Requests\RequestTest;

/**
 * A test case for \ShrooPHP\Framework\Request\Validators\MethodValidator.
 */
class MethodValidatorTest extends TestCase
{
	/**
	 * Asserts that the constructor of the method validator has expected
	 * behavior.
	 */
	public function testConstructor()
	{
		$methods = array('first', 'second');
		$validator = new MethodValidator($methods);

		foreach ($methods as $method) {
			$this->assertTrue($validator->validate($this->toRequest($method)));
		}

		$this->assertFalse($validator->validate($this->toRequest('third')));

	}

	/**
	 * Asserts that the constructor of the method validator has expected
	 * behavior when given an empty array.
	 */
	public function testConstructorWithEmptyArray()
	{
		$methods = array('first', 'second', 'third');
		$validator = new MethodValidator(array());

		foreach ($methods as $method) {
			$this->assertFalse($validator->validate($this->toRequest($method)));
		}
	}

	/**
	 * Converts the given method to a request.
	 *
	 * @param string $method the method to convert
	 * @return \ShrooPHP\Core\Request the converted method
	 */
	private function toRequest($method)
	{
		return new MethodRequest(RequestTest::create(), $method);
	}
}
