<?php

namespace ShrooPHP\Framework\Tests\Request\Validators;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Validator;
use ShrooPHP\Framework\Request\Validators\CallbackAdapter;
use ShrooPHP\Framework\Tests\Requests\RequestTest;

/**
 * A test case for \ShrooPHP\Framework\Request\Validators\CallbackAdapter.
 */
class CallbackAdapterTest extends TestCase implements Validator
{
	/**
	 * @var bool the current validity of the validator
	 */
	private $validity;

	public function validate(Request $request)
	{
		return $this->validity;
	}

	/**
	 * Sets up each test by initializing the validity of the validator.
	 */
	public function setUp()
	{
		$this->validity = false;
	}

	/**
	 * Asserts that the constructor of the adapter has expected behavior.
	 */
	public function testConstructor()
	{
		$adapter = new CallbackAdapter($this->toCallback());
		$this->assertCallbackAdapter($adapter, false);
		$this->assertCallbackAdapter($adapter, true);
	}

	/**
	 * Generates a callback for the current test.
	 *
	 * @return callable the generated callback
	 */
	private function toCallback()
	{
		return array($this, 'validate');
	}

	/**
	 * Asserts the given adapter.
	 *
	 * @param \ShrooPHP\Framework\Request\Validators\CallbackAdapter $adapter
	 * the adapter to assert
	 * @param bool the validity to assert
	 */
	private function assertCallbackAdapter(CallbackAdapter $adapter, $validity)
	{
		$request = RequestTest::create();

		$this->validity = $validity;

		$this->assertEquals($this->validity, $adapter->validate($request));
	}
}
