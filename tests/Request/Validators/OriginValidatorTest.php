<?php

namespace ShrooPHP\Framework\Tests\Request\Validators;

use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Framework\Request\Validators\OriginValidator;
use ShrooPHP\Framework\Tests\Requests\RequestTest;

/**
 * A test case for \ShrooPHP\Framework\Request\Validators\OriginValidator.
 */
class OriginValidatorTest extends TestCase
{
	/**
	 * The origin that is considered valid.
	 */
	const VALID = 'valid.example.org';

	/**
	 * An origin that is considered invalid.
	 */
	const INVALID = 'invalid.example.org';

	/**
	 * Asserts that the validator has expected behavior when configured via the
	 * constructor and given a request with a valid origin.
	 */
	public function testConstructorWithValidOrigin()
	{
		$validator = new OriginValidator(self::VALID);

		$this->assertOriginValidatorWithValidOrigin($validator);
	}

	/**
	 * Asserts that the validator has expected behavior when configured via the
	 * constructor and given a request with an invalid origin.
	 */
	public function testConstructorWithInvalidOrigin()
	{
		$validator = new OriginValidator(self::VALID);

		$this->assertOriginValidatorWithInvalidOrigin($validator);
	}

	/**
	 * Asserts that the given validator has expected behavior when given
	 * requests from valid origins.
	 */
	private function assertOriginValidatorWithValidOrigin(
		OriginValidator $validator
	) {
		$origin = $this->toHeadersWithOrigin(self::VALID);
		$referer = $this->toHeadersWithReferer(self::VALID);

		$this->assertOriginValidator($validator, $origin, true);
		$this->assertOriginValidator($validator, $referer, true);
	}

	/**
	 * Asserts that the given validator has expected behavior when given
	 * requests from invalid origins.
	 */
	private function assertOriginValidatorWithInvalidOrigin(
		OriginValidator $validator
	) {
		$origin = $this->toHeadersWithOrigin(self::INVALID);
		$referer = $this->toHeadersWithReferer(self::INVALID);

		$this->assertOriginValidator($validator, $origin, false);
		$this->assertOriginValidator($validator, $referer, false);
	}

	/**
	 * Asserts that the given validator has the given validity when given a
	 * request with the given headers.
	 */
	private function assertOriginValidator(
		OriginValidator $validator,
		ImmutableArrayObject $headers,
		$validity
	) {
		$request = RequestTest::create();
		$request->setHeaders($headers);
		$this->assertEquals($validity, $validator->validate($request));
	}

	/**
	 * Converts the given origin to a list of headers containing the given
	 * origin.
	 *
	 * @param mixed $origin the origin to convert
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the converted origin
	 */
	private function toHeadersWithOrigin($origin)
	{
		return $this->toHeaders(__METHOD__, $origin);
	}

	/**
	 * Converts the given referer to a list of headers containing the given
	 * referer.
	 *
	 * @param mixed $referer the referer to convert
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the converted referer
	 */
	private function toHeadersWithReferer($referer)
	{
		return $this->toHeaders($referer);
	}

	/**
	 * Converts the specified headers to a list of headers containing the
	 * given origin and referer.
	 *
	 * @param mixed $referer the referer to convert
	 * @param mixed $origin the origin to convert (if any)
	 * @return \ShrooPHP\Core\ReadOnlyArrayObject the converted referer and
	 * origin
	 */
	private function toHeaders($referer, $origin = null)
	{
		$headers = array('Referer' => $referer);

		if (!is_null($origin)) {
			$headers['Origin'] = $origin;
		}

		return new ImmutableArrayObject($headers);
	}
}
