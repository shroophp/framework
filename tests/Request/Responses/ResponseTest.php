<?php

namespace ShrooPHP\Framework\Tests\Request\Responses;

use EmptyIterator;
use ShrooPHP\Core\KeyValuePairIterator;
use ShrooPHP\Core\Pairs\Pair;
use ShrooPHP\Core\Request\Response as IResponse;
use ShrooPHP\Core\Request\Response\Presenter;
use ShrooPHP\Core\Runnable;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\Framework\Requests\Request;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Tests\Request\Responses\Response.
 */
class ResponseTest extends TestCase implements Presenter, Runnable
{
	/**
	 * @var int the currently expected response code
	 */
	private $code;

	/**
	 * @var \Traversable|array the currently expected headers
	 */
	private $headers;

	/**
	 * @var \Runnable the currently expected content
	 */
	private $content;

	/**
	 * @var \ShrooPHP\Core\Request\Response the response that is expected to be
	 * presented
	 */
	private $expected;

	/**
	 * @var string|null the expected output of the response content (or NULL to
	 * assert the response content itself instead)
	 */
	private $string;

	/**
	 * @var int the number of times that a response has been presented during
	 * the current test
	 */
	private $presented;

	/**
	 * Asserts that the given response is expected.
	 *
	 * @param \ShrooPHP\Core\Request\Response $response the response to assert
	 */
	public function present(IResponse $response)
	{
		$this->assertSame($this->expected, $response);
		$this->presented++;
	}

	/**
	 * Sets up each test by initializing the expected response code, the
	 * expected response headers, the expected response content, the expected
	 * response, the expected output of the content, and the number of times
	 * that a response has been presented.
	 */
	public function setUp()
	{
		$headers = array(
			new Pair('Content-Type', 'text/plain'),
			new Pair('X-Powered-By', 'Monster Energy Drinks'),
		);
		$this->code = Response::HTTP_OK;
		$this->headers = new KeyValuePairIterator($headers);
		$this->content = $this;
		$this->expected = null;
		$this->string = null;
		$this->presented = 0;
	}

	/**
	 * Asserts that a response created via a callback is formed correctly.
	 */
	public function testCallback()
	{
		$callback = array($this, 'file');
		$this->nullify();
		$this->string = __FILE__;

		$this->assertResponse(Response::callback($callback));
	}

	/**
	 * Asserts that a response created via a callback and a content type is
	 * formed correctly.
	 */
	public function testCallbackWithType()
	{
		$callback = array($this, 'file');
		$type = 'application/callback';

		$this->code = null;
		$this->headers = $this->toHeaders($type);
		$this->string = __FILE__;

		$this->assertResponse(Response::callback($callback, $type));
	}

	/**
	 * Asserts that a response created via a runnable is formed correctly.
	 */
	public function testCallbackFromRunnable()
	{
		$this->nullify();
		$this->content = $this;

		$this->assertResponse(Response::callback($this));
	}

	/**
	 * Asserts that a response created via a runnable and a content type is
	 * formed correctly.
	 */
	public function testCallbackFromRunnableWithType()
	{
		$type = 'application/runnable';

		$this->code = null;
		$this->headers = $this->toHeaders($type);

		$this->assertResponse(Response::callback($this, $type));
	}

	/**
	 * Asserts that a response created via a string is formed correctly.
	 */
	public function testString()
	{
		$this->nullify();
		$this->string = 'string';

		$this->assertResponse(Response::string($this->string));
	}

	/**
	 * Asserts that a response created via a string and a content type is
	 * formed correctly.
	 */
	public function testStringWithType()
	{
		$type = 'application/string';

		$this->nullify();
		$this->headers = $this->toHeaders($type);
		$this->string = 'string';

		$this->assertResponse(Response::string($this->string, $type));
	}

	/**
	 * Asserts that the constructor of the response has expected behavior.
	 */
	public function testContructor()
	{
		$this->assertResponse();
	}

	/**
	 * Asserts that the setters f the response have expected behavior.
	 */
	public function testSetters()
	{
		$response = new Response;
		$this->assertResponse($this->modify($response));
		$this->nullify();
		$this->assertResponse($this->modify($response));
	}

	/**
	 * Asserts that headers are implicitly converted to an iterator when
	 * specified as an array.
	 */
	public function testHeadersAsArray()
	{
		$this->nullify();
		$response = $this->toResponse();
		$this->headers = array(
			'Content-Type' => 'text/plain',
			'X-Powered-By' => 'Monster Energy Drinks',
		);

		// Assert that the constructor has expected behaviour.
		$this->assertResponse(new Response(null, $this->headers));

		// Assert that the setter has expected behaviour.
		$this->assertResponse($response->resetHeaders($this->headers));
	}

	/**
	 * Asserts that individual headers can be added to the response.
	 */
	public function testAddHeader()
	{
		$this->nullify();
		$response = $this->toResponse();
		$this->headers = array(
			'Content-Type' => 'text/plain',
			'X-Powered-By' => 'Monster Energy Drinks',
		);

		foreach ($this->headers as $header => $value) {
			$response->addHeader($header, $value);
		}

		$this->assertResponse($response);
	}

	/**
	 * Asserts that headers with the same name will not overwrite one another.
	 */
	public function testSimilarHeaders()
	{
		$headers = new KeyValuePairIterator(
			array(
				new Pair('Set-Cookie', 'type=milk-chocolate'),
				new Pair('Set-Cookie', 'type=oat-and-raisin'),
			)
		);

		// Assert similar headers via the constructor.
		$this->nullify();
		$this->headers = $headers;
		$this->assertResponse();

		// Assert similar headers via the setter.
		$response = new Response;
		$response->resetHeaders($headers);
		$this->assertResponse($response);

		// Assert similar headers by adding them individually.
		$response = new Response;
		foreach ($headers as $header => $value) {
			$this->assertSame($response, $response->addHeader($header, $value));
		}
		$this->assertResponse($response);
	}

	/**
	 * Asserts that running the response causes it to present itself.
	 */
	public function testRun()
	{
		$this->expected = $this->toResponse();

		$this->expected->run();
		$this->assertEquals(1, $this->presented);
	}

	/**
	 * Asserts that handling the request causes it to to present itself and
	 * return TRUE.
	 */
	public function testHandle()
	{
		$this->expected = $this->toResponse();
		$this->assertTrue($this->expected->handle(new Request('', '')));
		$this->assertEquals(1, $this->presented);
	}

	/**
	 * Asserts that responding to a request causes the response to return
	 * itself.
	 */
	public function testRespond()
	{
		$request = new Request('', '');
		$expected = $this->toResponse();
		$actual = $expected->respond($request, array());

		$this->assertSame($expected, $actual);
	}

	/**
	 * Asserts that the response modifies applications as expected.
	 */
	public function testModify()
	{
		$app = new Application;
		$this->expected = $this->toResponse();

		$app->path('', $this->expected);
		$this->assertTrue($app->handle(new Request('', '')));
		$this->assertEquals(1, $this->presented);
	}

	/**
	 * Renders the full path and filename of this file.
	 *
	 * This is primarily used to test the construction of responses via
	 * callbacks.
	 */
	public function file()
	{
		echo __FILE__;
	}

	/**
	 * Asserts that the given responses matches the current state of the test
	 * case.
	 *
	 * @param \ShrooPHP\Core\Request\Response $response the response to assert
	 */
	private function assertResponse(IResponse $response = null)
	{
		if (is_null($response)) {
			$response = $this->toResponse();
		}

		$content = $response->content();
		$this->assertEquals($this->code, $response->code());
		$this->assertEqualValues($this->headers, $response->headers());
		if (is_null($this->string)) {
			$this->assertEquals($this->content, $content);
		} else {
			ob_start();
			if (!is_null($content)) {
				$content->run();
			}
			$this->assertEquals($this->string, ob_get_clean());
		}
	}

	/**
	 * Asserts that the keys and values in each of the iterables are equal.
	 *
	 * @param \Traversable|array $expected
	 * @param \Traversable|array $actual
	 */
	private function assertEqualValues($expected, $actual)
	{
		$expecteds = array();
		$actuals = array();

		foreach ($expected as $header => $value) {
			$expecteds[] = array($header, $value);
		}

		foreach ($actual as $header => $value) {
			$actuals[] = array($header, $value);
		}

		$this->assertEquals($expecteds, $actuals);
	}

	/**
	 * Nullifies the currently expected values of the response.
	 */
	private function nullify()
	{
		$this->code = null;
		$this->headers = new EmptyIterator;
		$this->content = null;
		$this->expected = null;
	}

	/**
	 * Converts the given content type to headers.
	 *
	 * @param string $type the content type to convert
	 * @return \Traversable the converted content type
	 */
	private function toHeaders($type)
	{
		return new KeyValuePairIterator(array(new Pair('Content-Type', $type)));
	}

	/**
	 * Converts the test case to a response.
	 *
	 * @return \ShrooPHP\Framework\Request\Responses\Response the converted
	 * test case
	 */
	private function toResponse()
	{
		return new Response($this->code, $this->headers, $this->content, $this);
	}

	/**
	 * Modify the given response according to the current state of the test
	 * case.
	 *
	 * @param \ShrooPHP\Framework\Request\Responses\Response $response the
	 * response to modify
	 * @return \ShrooPHP\Framework\Request\Responses\Response the modified
	 * response
	 */
	private function modify(Response $response)
	{
		$this->assertSame($response, $response->setCode($this->code));
		$this->assertSame($response, $response->resetHeaders($this->headers));
		$this->assertSame($response, $response->setContent($this->content));
		$this->assertSame($response, $response->setPresenter($this));

		return $response;
	}
}
