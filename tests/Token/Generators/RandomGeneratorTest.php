<?php

namespace ShrooPHP\Framework\Tests\Token\Generators;

use ShrooPHP\Core\Token\Generator;
use ShrooPHP\Framework\Token\Generators\RandomGenerator;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Token\Generators\RandomGenerator.
 */
class RandomGeneratorTest extends TestCase
{
	/**
	 * The number of bytes being used to assert the random generator.
	 */
	const BYTES = 16;

	/**
	 * Asserts that the given token is formed of the given number of bytes
	 * within the given test case.
	 *
	 * @param \PHPUnit\Framework\TestCase $test the test case to assert against
	 * @param string $token the token to evaluate
	 * @param int $bytes the number of bytes to assert
	 */
	public static function assert(TestCase $test, $token, $bytes) {

		$length = $bytes * 2;
		$regex = "/^[a-z0-9]{{$length}}$/";

		$test->assertEquals(1, preg_match($regex, $token));
	}

	/**
	 * Asserts that the constructor of the random generator has expected
	 * behavior.
	 */
	public function testConstructor()
	{
		$generator = new RandomGenerator(self::BYTES);
		$this->assertGenerator($generator, self::BYTES);
	}

	/**
	 * Asserts that the given generator generates a token that is formed of the
	 * given number of bytes.
	 *
	 * @param \ShrooPHP\Core\Token\Generator $generator the generator to
	 * evaluate
	 * @param int $bytes the number of bytes to assert
	 */
	private function assertGenerator(Generator $generator, $bytes)
	{
		self::assert($this, $generator->generate(), $bytes);
	}
}
