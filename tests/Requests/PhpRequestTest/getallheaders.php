<?php

use ShrooPHP\Framework\Tests\Requests\PhpRequestTest;

/**
 * Generate mock headers as if fetched from the HTTP request.
 *
 * @return array the generated mock headers
 */
function getallheaders()
{
	return PhpRequestTest::headers();
}
