<?php

namespace ShrooPHP\Framework\Tests\Requests;

use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Requests\MethodRequest;
use ShrooPHP\Framework\Tests\Requests\RequestTest;
use PHPUnit\Framework\TestCase;

/**
 * A test case for ShrooPHP\Framework\Requests\MethodRequest.
 */
class MethodRequestTest extends TestCase
{
	/**
	 * @var \ShrooPHP\Core\Request the expected request
	 */
	private $request;

	/**
	 * @var string the expected method
	 */
	private $method;

	/**
	 * Sets up each test by initializing the expected request and the expected
	 * method.
	 */
	public function setUp()
	{
		$this->request = RequestTest::create();
		$this->method = __CLASS__;
	}

	/**
	 * Asserts that the constructor of the request has expected behaviour.
	 */
	public function testConstructor()
	{
		$request = new MethodRequest($this->request, $this->method);
		$this->assertMethodRequest($request);
	}

	/**
	 * Asserts the state of the given request.
	 *
	 * @param \ShrooPHP\Framework\Requests\MethodRequest $request the request
	 * to assert
	 */
	private function assertMethodRequest(MethodRequest $request)
	{
		RequestTest::assert($this, $this->request, $request, array(
			RequestTest::OVERRIDE_METHOD => $this->method
		));
	}
}
