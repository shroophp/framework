<?php

namespace ShrooPHP\Framework\Tests\Requests;

use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Core\Openable;
use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\Session as ISession;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Sessions\Session;
use ShrooPHP\Framework\Tokens\EmptyToken;
use ShrooPHP\Framework\Uploads\Upload;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Requests\Request.
 */
class RequestTest extends TestCase implements ISession, Openable
{
	/**
	 * The URL of the stream representing the test body.
	 */
	const BODY = 'php://memory';

	/**
	 * The flag identifying a request method override.
	 */
	const OVERRIDE_METHOD = 1;

	/**
	 * The flag identifying a request arguments override.
	 */
	const OVERRIDE_ARGS = 2;

	/**
	 * The flag identifying a request token override.
	 */
	const OVERRIDE_TOKEN = 3;

	/**
	 * @var string the method of the request
	 */
	private $method;

	/**
	 * @var string the path of the request
	 */
	private $path;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the URL parameters of the
	 * request
	 */
	private $params;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the headers of the request
	 */
	private $headers;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the cookies of the request
	 */
	private $cookies;

	/**
	 * @var \ShrooPHP\Core\Session the session associated with the
	 * request
	 */
	private $session;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the data associated with the
	 * body of the request
	 */
	private $data;

	/**
	 * @var array the files associated with the request
	 */
	private $uploads;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the arguments associated with
	 * the request
	 */
	private $args;

	/**
	 * @var \ShrooPHP\Core\Token the token associated with the request
	 */
	private $token;

	/**
	 * Creates a request.
	 *
	 * @return \ShrooPHP\Framework\Requests\Request the created request.
	 */
	public static function create()
	{
		$upload = new Upload('name', 'type', 1, 'path', UPLOAD_ERR_OK);

		return new Request(
			'method',
			'path',
			new ImmutableArrayObject(array('param' => 'param')),
			new ImmutableArrayObject(array('header' => 'header')),
			new ImmutableArrayObject(array('cookie' => 'cookie')),
			new Session,
			new self,
			new ImmutableArrayObject(array('data' => 'data')),
			array('upload' => $upload),
			new ImmutableArrayObject(array('arg' => 'arg')),
			new EmptyToken
		);
	}

	/**
	 * Performs a deep evaluation in order to assert whether or not the given
	 * requests are equal.
	 *
	 * @param \PHPUnit\Framework\TestCase $test the test case to assert against
	 * @param \ShrooPHP\Core\Request $expected the expected request state
	 * @param \ShrooPHP\Core\Request $actual the actual request state
	 * @param array $overrides the values to override within the expected
	 * request, keyed by the value of the respective override class constant
	 */
	public static function assert(
		TestCase $test,
		IRequest $expected,
		IRequest $actual,
		array $overrides = array()
	) {

		$container = new ImmutableArrayObject($overrides);
		$method = $container->get(self::OVERRIDE_METHOD, $expected->method());
		$args = $container->get(self::OVERRIDE_ARGS, $expected->args());
		$token = $container->get(self::OVERRIDE_TOKEN, $expected->token());

		$expected_stream = $expected->open();
		$actual_stream = $actual->open();

		$test->assertEquals($method, $actual->method());
		$test->assertEquals($expected->path(), $actual->path());
		$test->assertSame($expected->params(), $actual->params());
		$test->assertSame($expected->headers(), $actual->headers());
		$test->assertSame($expected->cookies(), $actual->cookies());
		$test->assertSame($expected->session(), $actual->session());
		$test->assertEquals(
			stream_get_meta_data($expected_stream),
			stream_get_meta_data($actual_stream)
		);
		$test->assertSame($expected->data(), $actual->data());
		$test->assertEquals($expected->uploads(), $actual->uploads());
		$test->assertSame($args, $actual->args());
		$test->assertSame($token, $actual->token());
		$test->assertSame($expected->root(), $actual->root());

		fclose($expected_stream);
		fclose($actual_stream);
	}

	public function get($key, $default = null)
	{
		return $default;
	}

	public function set($key, $value)
	{
		// Do nothing.
	}

	public function commit()
	{
		// Do nothing.
	}

	public function open()
	{
		$body = fopen(self::BODY, 'rb');

		return is_resource($body) ? $body : null;
	}

	/**
	 * Sets up each test by initializing the expected values of the request.
	 */
	public function setUp()
	{
		$this->method = 'method';
		$this->path = 'path';
		$this->params = new ImmutableArrayObject(array('get' => 'get'));
		$this->headers = new ImmutableArrayObject(array('header' => 'header'));
		$this->cookies = new ImmutableArrayObject(array('cookie' => 'cookie'));
		$this->session = $this;
		$this->data = new ImmutableArrayObject(array('post' => 'post'));
		$this->uploads = array('upload' => 'upload');
		$this->args = new ImmutableArrayObject(array('arg' => 'arg'));
		$this->token = new EmptyToken;
	}

	/**
	 * Asserts that the relevant helper method is able to successfully convert
	 * a request URI to a request path.
	 */
	public function testToPath()
	{
		$path = '/request/uri';
		$this->assertEquals($path, Request::toPath("{$path}?some=params"));
	}

	/**
	 * Asserts that the constructor of the request has expected behavior.
	 */
	public function testConstructor()
	{
		$this->assertRequest(null, $this);
	}

	/**
	 * Asserts that the setters of the request have expected behavior.
	 */
	public function testSetters()
	{
		$request = new Request('', '');
		$this->assertRequest($this->modify($request), $this);
	}

	/**
	 * Asserts the default values of a request.
	 */
	public function testDefaultValues()
	{
		$session = '\ShrooPHP\Framework\Sessions\Session';
		$args = '\ShrooPHP\Core\ReadOnlyArrayObject';
		$token = '\ShrooPHP\Framework\Tokens\EmptyToken';

		$request = new Request('', '');

		$expected = fopen(Request::BODY, 'rb');
		$actual = $request->open();

		$this->assertEquals(array(), $request->params()->getArrayCopy());
		$this->assertEquals(array(), $request->headers()->getArrayCopy());
		$this->assertEquals(array(), $request->cookies()->getArrayCopy());
		$this->assertInstanceOf($session, $request->session());
		$this->assertEquals(
			stream_get_meta_data($expected),
			stream_get_meta_data($actual)
		);
		$this->assertEquals(array(), $request->data()->getArrayCopy());
		$this->assertEquals(array(), $request->uploads());
		$this->assertInstanceOf($args, $request->args());
		$this->assertInstanceOf($token, $request->token());

		fclose($expected);
		fclose($actual);
	}

	/**
	 * Asserts that a request conforms to the current state of the test case.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to assert (or NULL to
	 * assert the default
	 */
	private function assertRequest(
		Request $request = null,
		Openable $body = null
	) {
		if (is_null($request)) {
			$request = $this->toRequest();
		}

		$actual = $request->open();
		$expected = is_null($body)
			? fopen(Request::BODY, 'rb')
			: $body->open();

		$this->assertEquals($this->method, $request->method());
		$this->assertEquals($this->path, $request->path());
		$this->assertEquals($this->params, $request->params());
		$this->assertEquals($this->headers, $request->headers());
		$this->assertEquals($this->cookies, $request->cookies());
		$this->assertEquals($this->session, $request->session());
		$this->assertEquals(
			stream_get_meta_data($expected),
			stream_get_meta_data($actual)
		);
		$this->assertEquals($this->data, $request->data());
		$this->assertEquals($this->uploads, $request->uploads());
		$this->assertEquals($this->args, $request->args());
		$this->assertEquals($this->token, $request->token());

		fclose($expected);
		fclose($actual);
	}

	/**
	 * Modify the given request based on the current state of the test case.
	 *
	 * @param \ShrooPHP\Framework\Requests\Request $request the request to
	 * modify
	 * @return \ShrooPHP\Framework\Requests\Request the modified request
	 */
	private function modify(Request $request)
	{
		$this->assertEquals($request, $request->setMethod($this->method));
		$this->assertEquals($request, $request->setPath($this->path));
		$this->assertEquals($request, $request->setParams($this->params));
		$this->assertEquals($request, $request->setHeaders($this->headers));
		$this->assertEquals($request, $request->setCookies($this->cookies));
		$this->assertEquals($request, $request->setSession($this->session));
		$this->assertEquals($request, $request->setBody($this));
		$this->assertEquals($request, $request->setData($this->data));
		$this->assertEquals($request, $request->setUploads($this->uploads));
		$this->assertEquals($request, $request->setArgs($this->args));
		$this->assertEquals($request, $request->setToken($this->token));

		return $request;
	}

	/**
	 * Converts the test case to a request.
	 *
	 * @return \ShrooPHP\Framework\Requests\Request the generated request
	 */
	private function toRequest()
	{
		return new Request(
			$this->method,
			$this->path,
			$this->params,
			$this->headers,
			$this->cookies,
			$this->session,
			$this,
			$this->data,
			$this->uploads,
			$this->args,
			$this->token
		);
	}
}
