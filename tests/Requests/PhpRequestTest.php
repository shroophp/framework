<?php

namespace ShrooPHP\Framework\Tests\Requests;

use ShrooPHP\Framework\Requests\PhpRequest;
use ShrooPHP\Framework\Requests\PhpRequestException;
use ShrooPHP\Framework\Uploads\Upload;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Requests\PhpRequest.
 */
class PhpRequestTest extends TestCase
{
	/**
	 * @var array temporary storage of URL query string variables
	 */
	private static $get;

	/**
	 * @var array temporary storage of HTTP post variables
	 */
	private static $post;

	/**
	 * @var array temporary storage of HTTP cookies
	 */
	private static $cookie;

	/**
	 * @var array temporary storage of uploaded files
	 */
	private static $files;

	/**
	 * Sets up the test case by copying HTTP superglobals to temporary storage.
	 */
	public static function setUpBeforeClass()
	{
		self::$get = $_GET;
		self::$post = $_POST;
		self::$cookie = $_COOKIE;
		self::$files = $_FILES;
	}

	/**
	 * Tears down the test case by copying HTTP superglobals from temporary
	 * storage.
	 */
	public static function tearDownAfterClass()
	{
		$_GET = self::$get;
		$_POST = self::$post;
		$_COOKIE = self::$cookie;
		$_FILES = self::$files;
	}

	/**
	 * Generate an array of test headers.
	 *
	 * @return array the generated test headers
	 */
	public static function headers()
	{
		return array(
			'X-Powered-By' => 'A nice cuppa tea.',
			'Content-Length' => '123',
			'Content-Type' => 'text/plain',
		);
	}

	/**
	 * Sets up each test by populating the HTTP superglobals with arbitrary
	 * data.
	 */
	public function setUp()
	{
		$_GET = array('get' => 'param');
		$_POST = array('post' => 'data');
		$_COOKIE = array('cookie' => 'cookies');
		$_FILES = array(
			'file' => array(
				'name' => 'first',
				'type' => 'text/plain',
				'size' => 1,
				'tmp_name' => 'php://temp?0',
				'error' => 0,
			),
			'files' => array(
				'name' => array(
					'second',
					'deep' => array(
						'qeeb' => 'third',
						'forth',
					),
					'fifth',
				),
				'type' => array(
					'text/html',
					'deep' => array(
						'qeeb' => 'application/pdf',
						'image/gif',
					),
					'image/png',
				),
				'size' => array(
					2,
					'deep' => array(
						'qeeb' => 4,
						8,
					),
					16,
				),
				'tmp_name' => array(
					'php://temp?1',
					'deep' => array(
						'qeeb' => 'php://temp?2',
						'php://temp?3',
					),
					'php://temp?4',
				),
				'error' => array(
					1,
					'deep' => array(
						'qeeb' => 2,
						3,
					),
					4,
				),
			),
		);
	}

	/**
	 * Asserts that the constructor of the PHP request has expected behavior.
	 *
	 * @runInSeparateProcess
	 */
	public function testConstructor()
	{
		$method = 'method';
		$path = 'path';
		$this->load('getallheaders.php');
		$this->assertRequest(new PhpRequest($method, $path), $method, $path);
	}

	/**
	 * Asserts that headers are extracted from the SAPI as expected.
	 *
	 * @runInSeparateProcess
	 */
	public function testToHeaders()
	{
		$this->load('getallheaders.php');
		$this->assertTrue(function_exists('getallheaders'));
		$this->assertEquals(getallheaders(), PhpRequest::toHeaders());
	}

	/**
	 * Asserts that the expected exception is thrown when `getallheaders()` is
	 * undefined.
	 */
	public function testToHeadersUndefined()
	{
		$this->assertFalse(function_exists('getallheaders'));
		$this->assertException(PhpRequestException::UNDEFINED);
	}
	/**
	 * Asserts that the expected exception is thrown when `getallheaders()`
	 * returns an unexpected value.
	 */
	public function testToHeadersFailure()
	{
		$this->load('getallheaders-false.php');
		$this->assertTrue(function_exists('getallheaders'));
		$this->assertException(PhpRequestException::FAILURE);
	}

	/**
	 * Asserts that flattening of the HTTP file upload superglobal is achieved
	 * recursively and as expected.
	 */
	public function testToUploads()
	{
		$path = 'php://temp';
		$path2 = "{$path}?2";
		$uploads = array();

		$file = new Upload('first', 'text/plain', 1, "{$path}?0", 0);
		$fileszero = new Upload('second', 'text/html', 2, "{$path}?1", 1);
		$filesdeepqeeb = new Upload('third', 'application/pdf', 4, $path2, 2);
		$filesdeepzero = new Upload('forth', 'image/gif', 8, "{$path}?3", 3);
		$fileslast = new Upload('fifth', 'image/png', 16, "{$path}?4", 4);

		$uploads['file'] = $file;
		$uploads['files[0]'] = $fileszero;
		$uploads['files[deep][qeeb]'] = $filesdeepqeeb;
		$uploads['files[deep][0]'] = $filesdeepzero;
		$uploads['files[1]'] = $fileslast;

		$this->assertEquals($uploads, PhpRequest::toUploads($_FILES));

	}

	/**
	 * Asserts that a PHP request exception with the given code is thrown
	 * during the retrieval of headers.
	 *
	 * @param int $code the exception code to assert
	 */
	private function assertException($code)
	{
		$exception = null;

		try {
			PhpRequest::toHeaders();
		} catch (PhpRequestException $exception) {
			$this->assertEquals($code, $exception->getCode());
		}

		$this->assertNotNull($exception);
	}

	/**
	 * Asserts the given request.
	 *
	 * @param \ShrooPHP\Framework\Requests\PhpRequest $request the request to
	 * assert
	 * @param string $method the expected method
	 * @param string $path the expected path
	 */
	private function assertRequest(PhpRequest $request, $method, $path)
	{
		$session = '\ShrooPHP\Framework\Sessions\Session';
		$token = '\ShrooPHP\Framework\Tokens\EmptyToken';

		$headers = $request->headers()->getArrayCopy();
		$uploads = $request->uploads();
		$body = $request->open();
		$stream = fopen(PhpRequest::BODY, 'rb');
		$metadata = stream_get_meta_data($stream);

		$this->assertEquals($method, $request->method());
		$this->assertEquals($path, $request->path());
		$this->assertEquals($_GET, $request->params()->getArrayCopy());
		$this->assertEquals(PhpRequest::toHeaders(), $headers);
		$this->assertEquals($_COOKIE, $request->cookies()->getArrayCopy());
		$this->assertInstanceOf($session, $request->session());
		$this->assertEquals($metadata, stream_get_meta_data($body));
		$this->assertEquals($_POST, $request->data()->getArrayCopy());
		$this->assertEquals(PhpRequest::toUploads($_FILES), $uploads);
		$this->assertEquals(array(), $request->args()->getArrayCopy());
		$this->assertInstanceOf($token, $request->token());
		$this->assertSame($request, $request->root());

		fclose($body);
		fclose($stream);
	}

	/**
	 * Requires the specified file.
	 *
	 * @param string $path the relative path to the file to require
	 */
	private function load($path)
	{
		$directory = substr(__CLASS__, strlen(__NAMESPACE__) + 1);

		require __DIR__ . DIRECTORY_SEPARATOR
			. $directory . DIRECTORY_SEPARATOR
			. $path;
	}
}

