<?php

namespace ShrooPHP\Framework\Tests\Requests;

use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Requests\ArgsRequest;
use PHPUnit\Framework\TestCase;

/**
 * A test case for \ShrooPHP\Framework\Requests\ArgsRequest.
 */
class ArgsRequestTest extends TestCase
{
	/**
	 * @var \ShrooPHP\Core\Request the expected request
	 */
	private $request;

	/**
	 * @var \ShrooPHP\Core\ReadOnlyArrayObject the expected arguments
	 */
	private $args;

	/**
	 * Sets up each test by initializing the expected request and arguments.
	 */
	public function setUp()
	{
		$args = array('arg1' => 'first', 'arg2' => 'second');

		$this->request = RequestTest::create();
		$this->args = new ImmutableArrayObject($args);
	}

	/**
	 * Asserts that the constructor of the request has expected behavior.
	 */
	public function testConstructor()
	{
		$request = new ArgsRequest($this->request, $this->args);
		$this->assertArgsRequest($request);
	}

	/**
	 * Asserts the state of the given request.
	 *
	 * @param \ShrooPHP\Framework\Requests\ArgsRequest $request the request to
	 * assert
	 */
	private function assertArgsRequest(ArgsRequest $request)
	{
		RequestTest::assert($this, $this->request, $request, array(
			RequestTest::OVERRIDE_ARGS => $this->args,
		));
	}
}
