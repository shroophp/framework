<?php

namespace ShrooPHP\Framework\Tests\Requests;

use ShrooPHP\Core\Token;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Requests\TokenRequest;
use ShrooPHP\Framework\Tests\Requests\RequestTest;
use ShrooPHP\Framework\Tokens\EmptyToken;
use PHPUnit\Framework\TestCase;

/**
 * A test case for ShrooPHP\Framework\Requests\TokenRequest.
 */
class TokenRequestTest extends TestCase implements Token
{
	/**
	 * The expected value of the token associated with the test case.
	 */
	const EXPECTED = __CLASS__;

	/**
	 * @var \ShrooPHP\Core\Request the expected request
	 */
	private $request;

	public function expected()
	{
		return __METHOD__;
	}

	public function valid()
	{
		return true;
	}

	/**
	 * Sets up each test by initializing the expected request.
	 */
	public function setUp()
	{
		$this->request = RequestTest::create();
	}

	/**
	 * Asserts that the constructor of the request has expected behaviour.
	 */
	public function testConstructor()
	{
		$request = new TokenRequest($this->request, $this);
		$this->assertTokenRequest($request);
	}

	/**
	 * Asserts the state of the given request.
	 *
	 * @param \ShrooPHP\Framework\Requests\TokenRequest $request the request
	 * to assert
	 */
	private function assertTokenRequest(TokenRequest $request)
	{
		RequestTest::assert($this, $this->request, $request, array(
			RequestTest::OVERRIDE_TOKEN => $this
		));
	}
}
